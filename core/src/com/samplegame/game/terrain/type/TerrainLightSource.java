package com.samplegame.game.terrain.type;

import com.samplegame.game.basic.type.Direction;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class TerrainLightSource {

    @NonNull
    private int row;

    @NonNull
    private int col;

    @NonNull
    private Direction direction;

    @NonNull
    private Float intensity;

    private boolean update = true;

}
