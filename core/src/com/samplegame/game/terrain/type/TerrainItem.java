package com.samplegame.game.terrain.type;

import com.samplegame.game.item.type.Item;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TerrainItem {

    private int col;

    private int row;

    private Item item;

    public String key() {
        return key(row, col);
    }

    public static String key(int row, int col) {
        return row + ":" + col;
    }

}
