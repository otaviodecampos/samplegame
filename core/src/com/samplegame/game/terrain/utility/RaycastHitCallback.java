package com.samplegame.game.terrain.utility;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.samplegame.SampleGame;
import com.samplegame.game.basic.component.DisposableComponent;
import com.samplegame.game.basic.component.LineComponent;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import lombok.AllArgsConstructor;

import java.util.function.Consumer;

import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.MeasureUtility.pixel;

@AllArgsConstructor
public class RaycastHitCallback implements RayCastCallback {

    private Body playerBody;

    private Body targetBody;

    private Consumer<Entity> consumer;

    @Override
    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
        if (SampleGame.debug) {
            createDebugLines(point);
        }
        Object userData = fixture.getBody().getUserData();
        if (userData instanceof Entity) {
            Entity entity = (Entity) userData;
            if (entity.getComponent(TerrainItemComponent.class) != null) {
                consumer.accept(entity);
            }
        }
        return 1;
    }

    private void createDebugLines(Vector2 point) {
        LineComponent line = createLine(0, Color.RED,
                new Vector2(pixel(playerBody.getPosition().x), pixel(playerBody.getPosition().y)),
                new Vector2(pixel(targetBody.getPosition().x), pixel(targetBody.getPosition().y)));
        engine.addEntity(EntityUtility.createEntity(engine, DisposableComponent.class, line));

        line = createLine(1, Color.GREEN,
                new Vector2(pixel(playerBody.getPosition().x), pixel(playerBody.getPosition().y)),
                new Vector2(new Vector2(pixel(point.x), pixel(point.y))));
        engine.addEntity(EntityUtility.createEntity(engine, DisposableComponent.class, line));
    }

    private LineComponent createLine(int layer, Color color, Vector2 startPosition, Vector2 endPosition) {
        LineComponent line = engine.createComponent(LineComponent.class);
        line.setColor(color);
        line.setStartPosition(startPosition);
        line.setEndPosition(endPosition);
        line.setLayer(layer);
        return line;
    }

}
