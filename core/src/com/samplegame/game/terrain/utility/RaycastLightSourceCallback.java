package com.samplegame.game.terrain.utility;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.type.TerrainItem;
import lombok.Getter;

public class RaycastLightSourceCallback implements RayCastCallback {

    @Getter
    private ObjectSet<TerrainItem> terrainItems = new ObjectSet<>();

    private ObjectMap<TerrainItem, Float> distances = new ObjectMap<>();

    @Override
    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
        Object userData = fixture.getBody().getUserData();
        if (userData instanceof Entity) {
            Entity entity = (Entity) userData;
            TerrainItemComponent terrainItemComponent = entity.getComponent(TerrainItemComponent.class);
            if (terrainItemComponent != null) {
                if (terrainItems.add(terrainItemComponent.getTerrainItem())) {
                    distances.put(terrainItemComponent.getTerrainItem(), fraction);
                }
            }
        }
        return 1;
    }

    public TerrainItem getClosest() {
        TerrainItem closestTerrainItem = null;
        Float minorFraction = null;
        for (ObjectMap.Entry<TerrainItem, Float> entry : distances.entries()) {
            if (minorFraction == null || entry.value < minorFraction) {
                closestTerrainItem = entry.key;
                minorFraction = entry.value;
            }
        }
        return closestTerrainItem;
    }

    public float getDistance(TerrainItem terrainItem) {
        return distances.get(terrainItem);
    }

    public RaycastLightSourceCallback clear() {
        terrainItems.clear();
        distances.clear();
        return this;
    }

}
