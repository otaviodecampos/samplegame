package com.samplegame.game.terrain.utility;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;

/**
 * Created by otavio on 18/03/2017.
 */
public class TerrainItemCursorUtility {

    public static int getCursorBlockCol(OrthographicCamera camera) {
        float x = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).x;
        if (x < 0) {
            x = x - TERRAIN_BLOCK_SIZE;
        }
        return (int) x / TERRAIN_BLOCK_SIZE;
    }

    public static int getCursorBlockRow(OrthographicCamera camera) {
        float y = camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0)).y;
        if (y < 0) {
            y = y - TERRAIN_BLOCK_SIZE;
        }
        return (int) y / TERRAIN_BLOCK_SIZE;
    }

    public static Vector3 getCursorInGameCamera(OrthographicCamera camera) {
        return camera.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
    }

}
