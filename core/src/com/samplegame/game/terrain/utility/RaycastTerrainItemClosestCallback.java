package com.samplegame.game.terrain.utility;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.utils.ObjectMap;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

@Getter
public class RaycastTerrainItemClosestCallback implements RayCastCallback {

    private ObjectMap<Float, Vector2> collisionPoints = new ObjectMap<>();

    @Override
    public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
        Object userData = fixture.getBody().getUserData();
        if (userData instanceof Entity) {
            Entity entity = (Entity) userData;
            if (entity.getComponent(TerrainItemComponent.class) != null) {
                collisionPoints.put(fraction, new Vector2(point.x, point.y));
            }
        }
        return 1;
    }

    public ArrayList<Vector2> getOrderedCollisionPoints() {
        TreeMap<Float, Vector2> map = new TreeMap<>();
        collisionPoints.forEach(entry -> map.put(entry.key, entry.value));
        return new ArrayList<>(map.values());
    }

    public Vector2 getFirstCollisionPoint() {
        List<Vector2> positions = getOrderedCollisionPoints();
        if (!positions.isEmpty()) {
            return positions.get(0);
        }
        return null;
    }

}
