package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.samplegame.GameConstants;
import com.samplegame.game.basic.type.Direction;
import com.samplegame.game.terrain.component.TerrainComponent;
import com.samplegame.game.terrain.component.TerrainLightComponent;
import com.samplegame.game.terrain.component.TerrainViewLimitComponent;
import com.samplegame.game.terrain.type.TerrainItem;
import com.samplegame.game.terrain.type.TerrainLightSource;
import com.samplegame.game.terrain.utility.RaycastLightSourceCallback;

import static com.samplegame.GameConstants.HALF_TERRAIN_BLOCK_SIZE;
import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.GameScreen.world;
import static com.samplegame.game.basic.utility.EntityUtility.findComponent;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class TerrainLightSystem extends EntitySystem {

    private TerrainComponent terrainComponent;

    private TerrainViewLimitComponent terrainViewLimitComponent;

    private TerrainLightComponent terrainLightComponent;

    private Vector2 lightSourcePosition = new Vector2();

    private Vector2 lightSourceTargetPosition = new Vector2();

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        terrainComponent = findComponent(engine, TerrainComponent.class);
        terrainViewLimitComponent = findComponent(engine, TerrainViewLimitComponent.class);
        terrainLightComponent = findComponent(engine, TerrainLightComponent.class);
    }

    @Override
    public void update(float deltaTime) {
        if (terrainLightComponent.isEmpty()) {
            buildTerrainLights();
            buildSunLight();
        }

        Array<String> chunkLoaded = new Array<>();
        for (int col = terrainViewLimitComponent.getMinCol(); col < terrainViewLimitComponent.getMaxCol(); col++) {
            for (int row = terrainViewLimitComponent.getMinRow(); row < terrainViewLimitComponent.getMaxRow(); row++) {
                String key = TerrainItem.key(row / 50, col / 50);
                if (!chunkLoaded.contains(key, false)) {
                    chunkLoaded.add(key);
                    for (TerrainLightSource terrainLightSource : terrainLightComponent.getChunkLightSource(row, col)) {
                        if (terrainLightSource.isUpdate() && updateLightSource(terrainLightSource)) {
                            terrainLightSource.setUpdate(false);
                        }
                    }
                }
            }
        }
    }

    private boolean updateLightSource(TerrainLightSource lightSource) {
        lightSourcePosition.x = meter(lightSource.getCol() * TERRAIN_BLOCK_SIZE + HALF_TERRAIN_BLOCK_SIZE);
        lightSourcePosition.y = meter(lightSource.getRow() * TERRAIN_BLOCK_SIZE + HALF_TERRAIN_BLOCK_SIZE);

        if (lightSource.getDirection() == Direction.BOTTOM) {
            lightSourceTargetPosition.x = meter(lightSource.getCol() * TERRAIN_BLOCK_SIZE + HALF_TERRAIN_BLOCK_SIZE);
            lightSourceTargetPosition.y = meter(-20 * TERRAIN_BLOCK_SIZE + HALF_TERRAIN_BLOCK_SIZE);
        }

        RaycastLightSourceCallback raycastLightSource = new RaycastLightSourceCallback();
        world.rayCast(raycastLightSource.clear(), lightSourcePosition, lightSourceTargetPosition);
        if (raycastLightSource.getTerrainItems().size > 10) {
            TerrainItem closestTerrainItem = raycastLightSource.getClosest();
            updateTerrainItemLight(closestTerrainItem.getRow(), closestTerrainItem.getCol(), lightSource.getIntensity());

            lightSourcePosition.x = meter(closestTerrainItem.getCol() * TERRAIN_BLOCK_SIZE + HALF_TERRAIN_BLOCK_SIZE);
            lightSourcePosition.y = meter(closestTerrainItem.getRow() * TERRAIN_BLOCK_SIZE + HALF_TERRAIN_BLOCK_SIZE);

            for (int a = 0; a < 360; a = a + 10) {
                lightSourceTargetPosition.x = (float) (lightSourcePosition.x + Math.cos(a) * meter(10 * TERRAIN_BLOCK_SIZE));
                lightSourceTargetPosition.y = (float) (lightSourcePosition.y + Math.sin(a) * meter(10 * TERRAIN_BLOCK_SIZE));

                /*LineComponent line = GameScreen.engine.createComponent(LineComponent.class);
                line.setColor(new Color(1f, 0f, 0f, 0.2f));
                line.setStartPosition(new Vector2(pixel(lightSourcePosition.x), pixel(lightSourcePosition.y)));
                line.setEndPosition(new Vector2(pixel(lightSourceTargetPosition.x), pixel(lightSourceTargetPosition.y)));
                line.setLayer(1);
                GameScreen.engine.addEntity(EntityUtility.createEntity(GameScreen.engine, line));*/

                raycastLightSource = new RaycastLightSourceCallback();
                world.rayCast(raycastLightSource, lightSourcePosition, lightSourceTargetPosition);
                for (TerrainItem terrainItem : raycastLightSource.getTerrainItems()) {
                    if (closestTerrainItem != terrainItem) {
                        updateTerrainItemLight(terrainItem.getRow(), terrainItem.getCol(), lightSource.getIntensity() - 0.2f - Math.nextDown(raycastLightSource.getDistance(terrainItem)));
                    }
                }
            }

            return true;
        }
        return false;
    }

    private void updateTerrainItemLight(int row, int col, float newLight) {
        Float currentLight = terrainLightComponent.getLight(row, col);
        if (currentLight != null && newLight <= currentLight) {
            return;
        }
        terrainLightComponent.setLight(row, col, newLight);
    }

    private float getLightResistance(int row, int col) {
        TerrainItem terrainItem = terrainComponent.getTerrainItem(row, col);
        return terrainItem == null ? 0f : 0.2f;
    }

    private void buildTerrainLights() {
        for (TerrainItem terrainItem : terrainComponent.getTerrainItems()) {
            terrainLightComponent.setLight(terrainItem.getRow(), terrainItem.getCol(), 0f);
        }
    }

    private void buildSunLight() {
        for (int col = 0; col < GameConstants.WORLD_SIZE.x; col++) {
            terrainLightComponent.addLightSource(new TerrainLightSource(20, col, Direction.BOTTOM, 1f));
        }
    }

}
