package com.samplegame.game.terrain.system.item;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.samplegame.game.basic.component.*;
import com.samplegame.game.basic.type.Random;
import com.samplegame.game.basic.utility.CacheUtility;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.component.item.TerrainItemTorchComponent;

import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.createEntity;
import static com.samplegame.game.basic.utility.MeasureUtility.*;

public class TerrainItemTorchSystem extends IteratingSystem {

    public TerrainItemTorchSystem() {
        super(Family.all(TerrainItemTorchComponent.class, TerrainItemComponent.class, TransformComponent.class, TextureComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        ParticleEffectComponent particleEffectComponent = entity.getComponent(ParticleEffectComponent.class);
        if (particleEffectComponent == null) {
            particleEffectComponent = engine.createComponent(ParticleEffectComponent.class);
            particleEffectComponent.setParticleEffect(createFlameParticle(entity.getComponent(TransformComponent.class)));
            entity.add(particleEffectComponent);
        }

        TerrainItemTorchComponent itemTorchComponent = entity.getComponent(TerrainItemTorchComponent.class);
        if (itemTorchComponent.getLightEntity() == null) {
            Entity lightEntity = createLightEntity(entity);
            itemTorchComponent.setLightEntity(lightEntity);
            engine.addEntity(lightEntity);
        } else {
            Entity lightEntity = itemTorchComponent.getLightEntity();
            StateComponent stateComponent = lightEntity.getComponent(StateComponent.class);
            if (stateComponent.getTime() > Random.next(0.1f, 0.5f)) {
                OpacityComponent opacityComponent = lightEntity.getComponent(OpacityComponent.class);
                opacityComponent.setOpacity(Random.next(0.5f, 1f));
                stateComponent.clear();

                TransformComponent transformComponent = lightEntity.getComponent(TransformComponent.class);
                if (Random.next(1f, 10f) <= 5f) {
                    transformComponent.getScale().x += 0.1f;
                    transformComponent.getScale().y += 0.1f;
                } else {
                    transformComponent.getScale().x -= 0.1f;
                    transformComponent.getScale().y -= 0.1f;
                }
                if (transformComponent.getScale().x < 0.9f) {
                    transformComponent.getScale().x = 0.9f;
                    transformComponent.getScale().y = 0.9f;
                } else if (transformComponent.getScale().x > 1.2f) {
                    transformComponent.getScale().x = 1.2f;
                    transformComponent.getScale().y = 1.2f;
                }
            }

        }
    }

    private Entity createLightEntity(Entity entity) {
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        texture.setRegion(CacheUtility.getTextureRegion("item/torch/light"));

        TransformComponent torchTransform = entity.getComponent(TransformComponent.class);
        TransformComponent transform = engine.createComponent(TransformComponent.class);
        transform.getPosition().x = torchTransform.getPosition().x;
        transform.getPosition().y = torchTransform.getPosition().y + meter(texture.getRegion().getRegionHeight());
        transform.setWidth(texture.getRegion().getRegionWidth());
        transform.setHeight(texture.getRegion().getRegionHeight());
        transform.setRotation(0f);
        transform.setIndex(3);

        OpacityComponent opacityComponent = engine.createComponent(OpacityComponent.class);
        opacityComponent.setOpacity(0.5f);

        StateComponent state = engine.createComponent(StateComponent.class);
        state.setState(0);

        return createEntity(getEngine(), texture, transform, opacityComponent, state);
    }

    private ParticleEffect createFlameParticle(TransformComponent torchTransform) {
        ParticleEffect particleEffect = CacheUtility.getParticleEffect("particles/torch_flame.particle", "particles");
        particleEffect.getEmitters().first().setPosition(pixel(torchTransform.getPosition().x - meter(5)), pixel(torchTransform.getPosition().y + meter(half(torchTransform.getHeight()))));
        particleEffect.start();
        return particleEffect;
    }

}
