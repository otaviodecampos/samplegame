package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.basic.component.*;
import com.samplegame.game.basic.type.Direction;
import com.samplegame.game.basic.type.Random;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.player.component.PlayerComponent;
import com.samplegame.game.terrain.component.TerrainComponent;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.component.TerrainLightComponent;
import com.samplegame.game.terrain.utility.RaycastHitCallback;
import com.samplegame.game.terrain.utility.RaycastTerrainItemClosestCallback;
import com.samplegame.game.ui.system.ItemBarSystem;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.GameScreen.world;
import static com.samplegame.game.basic.utility.CacheUtility.getTextureAtlas;
import static com.samplegame.game.basic.utility.EntityUtility.*;

public class TerrainItemHitSystem extends IteratingSystem {

    private TerrainComponent terrain;

    private PhysicComponent playerPhysic;

    private ItemBarSystem itemBarSystem;

    private TerrainLightComponent terrainLightComponent;

    public TerrainItemHitSystem() {
        super(Family.all(TerrainItemComponent.class, ClickableComponent.class).get());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        terrain = findComponent(engine, TerrainComponent.class);
        playerPhysic = findEntity(engine, PlayerComponent.class).getComponent(PhysicComponent.class);
        itemBarSystem = engine.getSystem(ItemBarSystem.class);
        terrainLightComponent = findComponent(engine, TerrainLightComponent.class);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        entity.remove(ClickableComponent.class);
        if (!itemBarSystem.isMouseDrag() && (terrain.getLastAddTime() == 0 || System.currentTimeMillis() - terrain.getLastAddTime() > 500)) {
            PhysicComponent blockPhysic = entity.getComponent(PhysicComponent.class);

            RaycastTerrainItemClosestCallback raycastClosest = new RaycastTerrainItemClosestCallback();
            world.rayCast(raycastClosest, playerPhysic.getBody().getPosition(), blockPhysic.getBody().getPosition());

            for (Vector2 collisionPoint : raycastClosest.getOrderedCollisionPoints()) {
                AtomicBoolean hit = new AtomicBoolean(false);
                world.rayCast(new RaycastHitCallback(playerPhysic.getBody(), blockPhysic.getBody(), blockEntity -> {
                    hit.set(true);
                    TerrainItemComponent block = blockEntity.getComponent(TerrainItemComponent.class);
                    if (block.hit()) {
                        terrain.remove(block.getTerrainItem());
                        engine.addEntity(createHitImpactEntity(blockEntity));
                        terrainLightComponent.updateChunkLightSource(block.getTerrainItem().getRow(), block.getTerrainItem().getCol(), false);
                    } else {
                        engine.addEntity(createHitEntity(blockEntity, block));
                    }
                }), playerPhysic.getBody().getPosition(), collisionPoint);
                if (hit.get()) {
                    //break;
                }
            }
        }
    }

    private Entity createHitEntity(Entity entity, TerrainItemComponent block) {
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        texture.setRegion(getHitTexture(block));

        TransformComponent blockTransform = entity.getComponent(TransformComponent.class);
        TransformComponent transform = engine.createComponent(TransformComponent.class);
        transform.getPosition().set(blockTransform.getPosition());
        transform.setWidth(blockTransform.getWidth());
        transform.setHeight(blockTransform.getHeight());
        transform.setRotation(0f);
        transform.setIndex(9);

        return createEntity(getEngine(), DisposableComponent.class, texture, transform);
    }

    private TextureRegion getHitTexture(TerrainItemComponent block) {
        int typeResistence = block.getTerrainItem().getItem().getResistance();
        int resistence = block.getResistance();
        int percentage = resistence * 100 / typeResistence;
        int index = 0;
        if (percentage < 90) {
            if (percentage < 25) {
                index = 3;
            } else if (percentage < 50) {
                index = 2;
            } else if (percentage < 75) {
                index = 1;
            }
        }
        return getTextureAtlas("block/block_break.atlas").findRegions("block_break").get(index);
    }

    private Entity createHitImpactEntity(Entity entity) {
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        texture.setRegion(null);
        StateComponent state = engine.createComponent(StateComponent.class);
        state.setState(0);
        RotationComponent rotation = engine.createComponent(RotationComponent.class);
        rotation.setFrameDuration(2f);
        List<Integer> directionIndexes = Arrays.asList(-1, 0, 1);
        rotation.setDirection(Direction.get(directionIndexes.get(Random.next(0, 3))));

        AnimationComponent animation = engine.createComponent(AnimationComponent.class);
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("player/assets.atlas"));
        Array<TextureAtlas.AtlasRegion> regions = atlas.findRegions("effect/hit");
        Animation idleAnimation = new Animation<>(0.1f, regions, Animation.PlayMode.NORMAL);
        animation.getAnimations().put(0, idleAnimation);

        TransformComponent blockTransform = entity.getComponent(TransformComponent.class);
        TransformComponent transform = engine.createComponent(TransformComponent.class);
        transform.getPosition().set(blockTransform.getPosition());
        transform.setWidth(TERRAIN_BLOCK_SIZE * 2);
        transform.setHeight(TERRAIN_BLOCK_SIZE * 2);
        transform.setIndex(10);

        return createEntity(getEngine(), DisposableComponent.class, animation, rotation, texture, transform, state);
    }

}
