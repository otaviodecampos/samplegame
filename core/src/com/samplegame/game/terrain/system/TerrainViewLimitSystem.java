package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.samplegame.game.basic.component.GameCameraComponent;
import com.samplegame.game.terrain.component.TerrainViewLimitComponent;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.basic.utility.EntityUtility.findComponent;

public class TerrainViewLimitSystem extends EntitySystem {

    private GameCameraComponent gameCamera;

    private TerrainViewLimitComponent terrainViewLimit;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        gameCamera = findComponent(engine, GameCameraComponent.class);
        terrainViewLimit = findComponent(engine, TerrainViewLimitComponent.class);
    }

    @Override
    public void update(float deltaTime) {
        int margin = TERRAIN_BLOCK_SIZE;
        int maxMargin = margin * 2;
        OrthographicCamera camera = gameCamera.getCamera();
        terrainViewLimit.setMinCol(Math.max((int) (camera.position.x - camera.zoom * camera.viewportWidth / 2 - margin) / TERRAIN_BLOCK_SIZE, 0));
        terrainViewLimit.setMaxCol((int) (camera.position.x + camera.zoom * camera.viewportWidth / 2 + maxMargin) / TERRAIN_BLOCK_SIZE);
        terrainViewLimit.setMinRow((int) (camera.position.y - camera.zoom * camera.viewportHeight / 2 - margin) / TERRAIN_BLOCK_SIZE);
        terrainViewLimit.setMaxRow((int) (camera.position.y + camera.zoom * camera.viewportHeight / 2 + maxMargin) / TERRAIN_BLOCK_SIZE);
    }


}
