package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.item.type.Item;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.component.TerrainItemRefreshComponent;

public class TerrainItemRefreshSystem extends IteratingSystem {

    public TerrainItemRefreshSystem() {
        super(Family.all(TerrainItemRefreshComponent.class, TerrainItemComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TerrainItemComponent terrainItem = entity.getComponent(TerrainItemComponent.class);
        terrainItem.getTerrainItem().setItem(Item.BROCK_TOP);
        entity.remove(TerrainItemRefreshComponent.class);
        entity.remove(TextureComponent.class);
    }

}
