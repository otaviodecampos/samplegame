package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Vector2;
import com.samplegame.game.basic.component.ColorComponent;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.utility.CacheUtility;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.item.type.ItemType;
import com.samplegame.game.physic.collision.CollisionCategory;
import com.samplegame.game.physic.collision.CollisionMask;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.physic.type.BodyModel;
import com.samplegame.game.physic.type.EdgeBodyModel;
import com.samplegame.game.terrain.component.*;
import com.samplegame.game.terrain.type.TerrainItem;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.findComponent;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

/**
 * Created by otavio on 14/03/2017.
 */
public class TerrainSystem extends EntitySystem {

    private EdgeBodyModel terrainBlockBodyModel = new EdgeBodyModel(false, true, null, 0.2f, null, CollisionCategory.GROUND_CATEGORY, CollisionMask.GROUND_MASK);

    private EdgeBodyModel terrainItemBodyModel = new EdgeBodyModel(true, true, null, null, null, CollisionCategory.GROUND_CATEGORY, CollisionMask.GROUND_MASK);

    private TerrainComponent terrain;

    private TerrainViewLimitComponent terrainViewLimit;

    private TerrainLightComponent terrainLightComponent;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        terrain = findComponent(engine, TerrainComponent.class);
        terrainViewLimit = findComponent(engine, TerrainViewLimitComponent.class);
        terrainLightComponent = findComponent(engine, TerrainLightComponent.class);
    }

    @Override
    public void update(float deltaTime) {
        for (int row = terrainViewLimit.getMinRow(); row < terrainViewLimit.getMaxRow(); row++) {
            for (int col = terrainViewLimit.getMinCol(); col < terrainViewLimit.getMaxCol(); col++) {
                TerrainItem terrainItem = terrain.getTerrainItem(row, col);
                if (terrainItem != null) {
                    Entity entity = terrain.getItemEntity(row, col);

                    if (entity == null) {
                        entity = engine.createEntity();
                        terrain.add(row, col, entity);
                        engine.addEntity(entity);
                    }

                    TextureComponent texture = entity.getComponent(TextureComponent.class);
                    if (texture == null) {
                        entity.add(createItemTextureComponent(terrainItem));
                    }

                    TerrainItemComponent itemComponent = entity.getComponent(TerrainItemComponent.class);
                    if (itemComponent == null) {
                        itemComponent = createItemComponent(terrainItem);
                        entity.add(itemComponent);
                    }

                    TransformComponent transformComponent = entity.getComponent(TransformComponent.class);
                    if (transformComponent == null) {
                        entity.add(createTransformComponent(terrainItem));
                    }

                    PhysicComponent physicComponent = entity.getComponent(PhysicComponent.class);
                    if (physicComponent == null) {
                        physicComponent = createItemPhysicComponent(terrainItem, itemComponent);
                        physicComponent.getBody().setUserData(entity);
                        entity.add(physicComponent);
                    }

                    if (terrainItem.getItem().getType().equals(ItemType.BLOCK) && !terrain.exists(terrainItem.getRow() + 1, terrainItem.getCol())) {
                        TerrainItemRefreshComponent terrainItemRefresh = entity.getComponent(TerrainItemRefreshComponent.class);
                        if (terrainItemRefresh == null) {
                            terrainItemRefresh = engine.createComponent(TerrainItemRefreshComponent.class);
                            entity.add(terrainItemRefresh);
                        }
                    }

                    TerrainItemVisibleComponent visibleComponent = entity.getComponent(TerrainItemVisibleComponent.class);
                    if (visibleComponent == null) {
                        visibleComponent = engine.createComponent(TerrainItemVisibleComponent.class);
                        entity.add(visibleComponent);
                    }
                    visibleComponent.setLastVisibleTime(System.currentTimeMillis());

                    Class<? extends Component> terrainItemComponentClass = terrainItem.getItem().getTerrainItemComponentClass();
                    if (terrainItemComponentClass != null && entity.getComponent(terrainItemComponentClass) == null) {
                        entity.add(engine.createComponent(terrainItemComponentClass));
                    }

                    ColorComponent colorComponent = entity.getComponent(ColorComponent.class);
                    Float terrainLight = terrainLightComponent.getLight(row, col);
                    if (terrainLight != null) {
                        if (colorComponent == null) {
                            colorComponent = EntityUtility.createComponent(getEngine(), ColorComponent.class);
                            entity.add(colorComponent);
                        }
                        colorComponent.set(terrainLight);
                    }
                }
            }
        }
    }

    private TerrainItemComponent createItemComponent(TerrainItem terrainItem) {
        TerrainItemComponent component = engine.createComponent(TerrainItemComponent.class);
        component.setTerrainItem(terrainItem);
        return component;
    }

    private TextureComponent createItemTextureComponent(TerrainItem terrainItem) {
        TextureComponent component = engine.createComponent(TextureComponent.class);
        component.setRegion(CacheUtility.getTextureRegion(terrainItem.getItem().getTexturePath()));
        return component;
    }

    private PhysicComponent createItemPhysicComponent(TerrainItem terrainItem, TerrainItemComponent block) {
        PhysicComponent component = engine.createComponent(PhysicComponent.class);
        BodyModel bodyModel = terrainItem.getItem().getType().equals(ItemType.BLOCK) ? terrainBlockBodyModel : terrainItemBodyModel;
        component.setBody(bodyModel.build(terrainItem.getItem().getWidth(), terrainItem.getItem().getHeight(), meter(block.getTerrainItem().getCol() * TERRAIN_BLOCK_SIZE), meter(block.getTerrainItem().getRow() * TERRAIN_BLOCK_SIZE)));
        return component;
    }

    private TransformComponent createTransformComponent(TerrainItem terrainItem) {
        TransformComponent component = engine.createComponent(TransformComponent.class);
        component.setWidth(terrainItem.getItem().getWidth());
        component.setHeight(terrainItem.getItem().getHeight());
        component.getPosition().set(new Vector2(meter(terrainItem.getCol() * TERRAIN_BLOCK_SIZE + half(terrainItem.getItem().getWidth())), meter(terrainItem.getRow() * TERRAIN_BLOCK_SIZE + half(terrainItem.getItem().getHeight()))));
        component.setIndex(4);
        return component;
    }

}

