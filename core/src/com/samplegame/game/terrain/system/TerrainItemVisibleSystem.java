package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.component.TerrainItemVisibleComponent;

public class TerrainItemVisibleSystem extends IteratingSystem {

    public TerrainItemVisibleSystem() {
        super(Family.all(TerrainItemComponent.class, TerrainItemVisibleComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        TerrainItemVisibleComponent itemVisibleComponent = entity.getComponent(TerrainItemVisibleComponent.class);
        if (System.currentTimeMillis() - itemVisibleComponent.getLastVisibleTime() >= 10) {
            entity.remove(TransformComponent.class);
            entity.remove(TerrainItemVisibleComponent.class);
        }
    }

}
