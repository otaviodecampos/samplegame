package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.samplegame.game.basic.component.ClickableComponent;
import com.samplegame.game.basic.component.GameCameraComponent;
import com.samplegame.game.basic.component.HoverComponent;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.terrain.component.TerrainComponent;
import com.samplegame.game.terrain.utility.TerrainItemCursorUtility;

import static com.samplegame.game.GameScreen.engine;

public class TerrainItemInputSystem extends EntitySystem {

    private GameCameraComponent gameCamera;

    private TerrainComponent terrain;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        gameCamera = EntityUtility.findComponent(engine, GameCameraComponent.class);
        terrain = EntityUtility.findComponent(engine, TerrainComponent.class);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);

        int col = TerrainItemCursorUtility.getCursorBlockCol(gameCamera.getCamera());
        int row = TerrainItemCursorUtility.getCursorBlockRow(gameCamera.getCamera());

        Entity entity = terrain.getItemEntity(row, col);

        if (entity != null) {
            entity.add(engine.createComponent(HoverComponent.class));
            if (Gdx.input.isTouched()) {
                entity.add(engine.createComponent(ClickableComponent.class));
            }
        }
    }

}
