package com.samplegame.game.terrain.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.math.Vector2;
import com.samplegame.game.basic.component.DisposableComponent;
import com.samplegame.game.basic.component.HoverComponent;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.utility.CacheUtility;
import com.samplegame.game.terrain.component.TerrainItemComponent;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.createEntity;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class TerrainItemHighlightSystem extends IteratingSystem {

    public TerrainItemHighlightSystem() {
        super(Family.all(TerrainItemComponent.class, HoverComponent.class).get());
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        engine.addEntity(createShapeEntity(entity.getComponent(TerrainItemComponent.class)));
        entity.remove(HoverComponent.class);
    }

    private Entity createShapeEntity(TerrainItemComponent block) {
        TextureComponent texture = engine.createComponent(TextureComponent.class);
        texture.setRegion(CacheUtility.getHighlightTextureRegion(block.getTerrainItem().getItem().getTexturePath()));

        TransformComponent transform = engine.createComponent(TransformComponent.class);
        transform.setWidth(block.getTerrainItem().getItem().getWidth());
        transform.setHeight(block.getTerrainItem().getItem().getHeight());
        transform.getPosition().set(new Vector2(meter(block.getTerrainItem().getCol() * TERRAIN_BLOCK_SIZE + half(block.getTerrainItem().getItem().getWidth())), meter(block.getTerrainItem().getRow() * TERRAIN_BLOCK_SIZE + half(block.getTerrainItem().getItem().getHeight()))));
        transform.setIndex(8);

        return createEntity(getEngine(), DisposableComponent.class, texture, transform);
    }

}
