package com.samplegame.game.terrain.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.samplegame.game.terrain.type.TerrainItem;
import com.samplegame.game.terrain.type.TerrainLightSource;

public class TerrainLightComponent implements Component {

    private final ObjectMap<String, ObjectMap<String, Float>> lights = new ObjectMap<>();

    private final ObjectMap<String, Array<TerrainLightSource>> lightSources = new ObjectMap<>();

    public Float getLight(int row, int col) {
        return getChunkLight(row, col).get(TerrainItem.key(row, col));
    }

    public void setLight(int row, int col, float light) {
        getChunkLight(row, col).put(TerrainItem.key(row, col), light);
    }

    public boolean isEmpty() {
        return lights.size == 0;
    }

    public void addLightSource(TerrainLightSource terrainLightSource) {
        getChunkLightSource(terrainLightSource.getRow(), terrainLightSource.getCol()).add(terrainLightSource);
    }

    private ObjectMap<String, Float> getChunkLight(int row, int col) {
        String key = TerrainItem.key(row / 50, col / 50);
        ObjectMap<String, Float> lightChunk = lights.get(key);
        if (lightChunk == null) {
            lightChunk = new ObjectMap<>();
            lights.put(key, lightChunk);
        }
        return lightChunk;
    }

    public Array<TerrainLightSource> getChunkLightSource(int row, int col) {
        String key = TerrainItem.key(row / 50, col / 50);
        Array<TerrainLightSource> lightChunkSources = lightSources.get(key);
        if (lightChunkSources == null) {
            lightChunkSources = new Array<>();
            lightSources.put(key, lightChunkSources);
        }
        return lightChunkSources;
    }

    public void updateChunkLightSource(int row, int col, boolean sideChunks) {
        int chunkRow = row / 50;
        int chunkCol = col / 50;
        for (TerrainLightSource terrainLightSource : lightSources.get(TerrainItem.key(chunkRow, chunkCol))) {
            terrainLightSource.setUpdate(true);
        }
        if (sideChunks) {
            updateChunkLightSource(chunkRow + 1, chunkCol, false);
            updateChunkLightSource(chunkRow - 1, chunkCol, false);
            updateChunkLightSource(chunkRow, chunkCol + 1, false);
            updateChunkLightSource(chunkRow, chunkCol - 1, false);
        }
    }

}
