package com.samplegame.game.terrain.component.item;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TerrainItemTorchComponent implements Component {

    private Entity lightEntity;

}
