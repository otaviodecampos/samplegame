package com.samplegame.game.terrain.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.utils.ObjectMap;
import com.samplegame.game.item.type.ItemType;
import com.samplegame.game.terrain.type.TerrainItem;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.terrain.type.TerrainItem.key;

/**
 * Created by otavio on 14/03/2017.
 */
public class TerrainComponent implements Component {

    private final ObjectMap<String, TerrainItem> items = new ObjectMap<>();

    private final ObjectMap<String, String> allocated = new ObjectMap<>();

    private final ObjectMap<String, Entity> entities = new ObjectMap<>();

    @Getter
    private long lastDestroyTime = 0;

    @Getter
    private long lastAddTime = 0;

    public ObjectMap.Values<TerrainItem> getTerrainItems() {
        return items.values();
    }

    public void remove(TerrainItem terrainItem) {
        String key = terrainItem.key();
        Entity entity = entities.get(key);
        if (entity != null) {
            entities.remove(key);
            items.remove(key);
            removeAllocatedKeys(key);
            engine.removeEntity(entity);
            lastDestroyTime = System.currentTimeMillis();
        }
    }

    public TerrainItem getTerrainItem(int row, int col) {
        return items.get(key(row, col));
    }

    public Entity getItemEntity(int row, int col) {
        String key = key(row, col);
        Entity entity = entities.get(key);
        if (entity == null) {
            key = allocated.get(key);
            if (key != null) {
                entity = entities.get(key);
            }
        }
        return entity;
    }

    public void add(int row, int col, Entity entity) {
        entities.put(key(row, col), entity);
    }

    public boolean add(TerrainItem terrainItem) {
        return add(terrainItem, true);
    }

    public boolean add(TerrainItem terrainItem, boolean check) {
        if (!check || !exists(terrainItem) && allowedToAdd(terrainItem)) {
            String key = terrainItem.key();
            items.put(key, terrainItem);
            for (int i = 1; i < terrainItem.getItem().getHeight() / TERRAIN_BLOCK_SIZE; i++) {
                String allocatedKey = key(terrainItem.getRow() + i, terrainItem.getCol());
                allocated.put(allocatedKey, key);
            }
            lastAddTime = System.currentTimeMillis();
            return true;
        }
        return false;
    }

    public boolean allowedToAdd(TerrainItem terrainItem) {
        if (terrainItem.getItem().getType().equals(ItemType.BLOCK)) {
            return exists(key(terrainItem.getRow() + 1, terrainItem.getCol()), ItemType.BLOCK)
                    || exists(key(terrainItem.getRow() - 1, terrainItem.getCol()), ItemType.BLOCK)
                    || exists(key(terrainItem.getRow(), terrainItem.getCol() + 1), ItemType.BLOCK)
                    || exists(key(terrainItem.getRow(), terrainItem.getCol() - 1), ItemType.BLOCK);
        } else {
            boolean allowed = exists(key(terrainItem.getRow() - 1, terrainItem.getCol()), ItemType.BLOCK);
            if (allowed) {
                int totalRows = terrainItem.getItem().getHeight() / TERRAIN_BLOCK_SIZE;
                for (int i = 0; i < totalRows; i++) {
                    if (exists(terrainItem.getRow() + i, terrainItem.getCol())) {
                        allowed = false;
                        break;
                    }
                }
            }
            return allowed;
        }
    }

    public boolean exists(String key, ItemType itemType) {
        TerrainItem item = items.get(key);
        return item != null && item.getItem().getType().equals(itemType);
    }

    public boolean exists(TerrainItem terrainItem) {
        return exists(terrainItem.getRow(), terrainItem.getCol());
    }

    public boolean exists(int row, int col) {
        String key = key(row, col);
        return items.get(key) != null || allocated.get(key) != null;
    }

    private void removeAllocatedKeys(String key) {
        for (String allocatedKey : findAllocatedKeysFor(key)) {
            allocated.remove(allocatedKey);
        }
    }

    private List<String> findAllocatedKeysFor(String key) {
        List<String> allocatedKeys = new ArrayList<>();
        for (ObjectMap.Entry<String, String> entry : allocated.entries()) {
            if (entry.value.equals(key)) {
                allocatedKeys.add(entry.key);
            }
        }
        return allocatedKeys;
    }

}
