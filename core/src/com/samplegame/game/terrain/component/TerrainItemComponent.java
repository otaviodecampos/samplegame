package com.samplegame.game.terrain.component;

import com.badlogic.ashley.core.Component;
import com.samplegame.game.basic.component.ClearableComponent;
import com.samplegame.game.terrain.type.TerrainItem;
import com.samplegame.game.item.type.Item;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TerrainItemComponent implements Component, ClearableComponent {

    private TerrainItem terrainItem;

    private int resistance;

    private long lastHitTime;

    public void setTerrainItem(TerrainItem terrainItem) {
        this.terrainItem = terrainItem;
        this.resistance = terrainItem.getItem().getResistance();
    }

    public boolean hit() {
        if (System.currentTimeMillis() - lastHitTime > 100) {
            resistance = getTerrainItem().getItem().getResistance();
        }
        lastHitTime = System.currentTimeMillis();
        return --resistance < 0;
    }

    @Override
    public void clear() {
        terrainItem = null;
    }

}
