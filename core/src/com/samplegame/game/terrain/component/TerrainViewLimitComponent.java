package com.samplegame.game.terrain.component;

import com.badlogic.ashley.core.Component;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TerrainViewLimitComponent implements Component {

    private int minCol;

    private int maxCol;

    private int minRow;

    private int maxRow;

}
