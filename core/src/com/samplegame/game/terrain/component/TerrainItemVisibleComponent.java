package com.samplegame.game.terrain.component;

import com.badlogic.ashley.core.Component;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TerrainItemVisibleComponent implements Component {

    private long lastVisibleTime;

}
