package com.samplegame.game.terrain.builder.noise;

import com.badlogic.gdx.utils.Array;

/**
 * Created by otavio on 08/03/2017.
 */
public class PerlinNoise1D {

    private static double interpolate(double pointA, double pointB, double pointX) {
        double ft = pointX * Math.PI,
                f = (1 - Math.cos(ft)) * 0.5;
        return pointA * (1 - f) + pointB * f;
    }

    private Array<Double> perlin(double seed, int amplitude, int wavelength, int width) {
        double x = 0;

        PsuedoRandomLinearCongruentialNumber randomLinearNumbers = new PsuedoRandomLinearCongruentialNumber(seed);
        double pointA = randomLinearNumbers.generate();
        double pointB = randomLinearNumbers.generate();

        Array<Double> positions = new Array<>();

        while (x < width) {
            if (x % wavelength == 0) {
                pointA = pointB;
                pointB = randomLinearNumbers.generate();
                positions.add(pointA * amplitude);
            } else {
                positions.add(interpolate(pointA, pointB, (x % wavelength) / wavelength) * amplitude);
            }
            x++;
        }

        return positions;
    }

    private Array<Double> combine(Array<Array<Double>> positions) {
        Array<Double> combined = new Array<>();

        for (Array<Double> list : positions) {
            for (Double position : list) {
                int index = list.indexOf(position, true);
                if (combined.size >= index + 1) {
                    position += combined.get(index);
                    combined.set(index, position);
                } else {
                    combined.add(position);
                }
            }
        }

        return combined;
    }

    public Array<Double> noise(double seed, int amplitude, int wavelength, int octaves, int divisor, int width) {
        Array<Array<Double>> positions = new Array<>();

        for (int i = 0; i < octaves; i++) {
            positions.add(perlin(seed, amplitude, wavelength, width));
            amplitude /= divisor;
            wavelength /= divisor;
        }

        return combine(positions);
    }

}
