package com.samplegame.game.terrain.builder.noise;

/**
 * Created by otavio on 08/03/2017.
 */
public class PsuedoRandomLinearCongruentialNumber {

    private static double MAX_LENGTH = 4294967296f;

    private static double SEED_MULTIPLIER_FACTOR = 1664525;

    private static double SEED_SUM_FACTOR = 1;

    private double seed;

    public PsuedoRandomLinearCongruentialNumber(Double seed) {
        if (seed == null || seed == 0) {
            seed = Math.floor(Math.random() * MAX_LENGTH);
        }
        this.seed = seed;
    }

    public double generate() {
        this.seed = Math.floor(Math.random() * MAX_LENGTH);
        this.seed = (SEED_MULTIPLIER_FACTOR * this.seed + SEED_SUM_FACTOR) % MAX_LENGTH;
        return this.seed / MAX_LENGTH - 0.5;
    }

}
