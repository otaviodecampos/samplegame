package com.samplegame.game.terrain.builder;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.item.type.Item;
import com.samplegame.game.terrain.builder.noise.PerlinNoise1D;
import com.samplegame.game.terrain.type.TerrainItem;
import lombok.Getter;

@Getter
public class TerrainBuilder {

    private int smallerRow = 0;

    public Array<TerrainItem> build(Vector2 size) {
        return build((int) size.x, (int) size.y);
    }

    private Array<TerrainItem> build(int maxCols, int maxRows) {
        Array<TerrainItem> blocks = new Array<>();

        Array<Integer> rows = buildRows(new PerlinNoise1D().noise(0, 10, 20, 2, 2, maxCols));
        for (int col = 0; col < rows.size; col++) {
            int row = rows.get(col) - smallerRow;

            TerrainItem block = new TerrainItem(col, row, Item.BROCK);
            blocks.add(block);

            int soilRow = block.getRow();
            while (soilRow > -maxRows) {
                soilRow--;
                blocks.add(new TerrainItem(block.getCol(), soilRow, Item.BROCK));
            }
        }

        return blocks;
    }

    private Array<Integer> buildRows(Array<Double> rowPositions) {
        Array<Integer> rows = new Array<>();
        rowPositions.forEach(rowPosition -> {
            int row = (int) Math.ceil(rowPosition);
            if (row < smallerRow) {
                smallerRow = row;
            }
            rows.add(row);
        });
        return rows;
    }

}
