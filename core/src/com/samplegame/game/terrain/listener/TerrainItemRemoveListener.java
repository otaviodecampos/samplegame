package com.samplegame.game.terrain.listener;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntityListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.type.Random;
import com.samplegame.game.basic.utility.CacheUtility;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.item.component.ItemComponent;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.player.component.PlayerComponent;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.component.item.TerrainItemTorchComponent;

import java.util.SplittableRandom;

import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.GameScreen.world;
import static com.samplegame.game.basic.utility.EntityUtility.createEntity;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;
import static com.samplegame.game.basic.utility.NumberUtility.invert;

public class TerrainItemRemoveListener implements EntityListener {

    @Override
    public void entityAdded(Entity entity) {

    }

    @Override
    public void entityRemoved(Entity entity) {
        PhysicComponent physicComponent = entity.getComponent(PhysicComponent.class);
        if (physicComponent != null) {
            world.destroyBody(physicComponent.getBody());
        }

        TerrainItemTorchComponent itemTorchComponent = entity.getComponent(TerrainItemTorchComponent.class);
        if (itemTorchComponent != null) {
            Vector2 scale = itemTorchComponent.getLightEntity().getComponent(TransformComponent.class).getScale();
            scale.x = 1f;
            scale.y = 1f;
            engine.removeEntity(itemTorchComponent.getLightEntity());
            itemTorchComponent.setLightEntity(null);
        }

        engine.addEntity(createBlockItemEntity(entity));
    }

    private Entity createBlockItemEntity(Entity blockEntity) {
        TerrainItemComponent block = blockEntity.getComponent(TerrainItemComponent.class);
        ItemComponent item = engine.createComponent(ItemComponent.class);
        item.setType(block.getTerrainItem().getItem());

        TransformComponent blockTransform = blockEntity.getComponent(TransformComponent.class);
        TransformComponent transform = engine.createComponent(TransformComponent.class);
        transform.getPosition().set(blockTransform.getPosition());
        transform.setWidth((int) half(blockTransform.getWidth()));
        transform.setHeight((int) half(blockTransform.getHeight()));

        TextureComponent texture = engine.createComponent(TextureComponent.class);
        texture.setRegion(CacheUtility.getTextureRegion(block.getTerrainItem().getItem().getTexturePath()));

        PhysicComponent physic = engine.createComponent(PhysicComponent.class);
        Body body = item.getType().getBodyModel().build(transform.getWidth(), transform.getHeight(), blockTransform.getPosition().x - meter(new SplittableRandom().nextInt(0, transform.getWidth())), blockTransform.getPosition().y);
        float forceX = invert(Random.next(0f, 3.00f), EntityUtility.findEntity(engine, PlayerComponent.class).getComponent(TransformComponent.class).getScaleDirection());
        body.applyForce(forceX, 0, body.getPosition().x, body.getPosition().y, true);
        physic.setBody(body);

        Entity entity = createEntity(engine, item, transform, texture, physic);
        body.setUserData(entity);
        return entity;
    }

}
