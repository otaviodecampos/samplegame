package com.samplegame.game.physic.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.physic.component.PhysicComponent;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;

public class PhysicSystem extends IteratingSystem {

    public PhysicSystem() {
        super(Family.all(TransformComponent.class, PhysicComponent.class).exclude(TerrainItemComponent.class).get());
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        TransformComponent transform = getComponent(entity, TransformComponent.class);
        PhysicComponent physic = getComponent(entity, PhysicComponent.class);
        transform.getPosition().set(physic.getBody().getPosition());
        transform.setRotation(physic.getBody().getAngle());
    }

}

