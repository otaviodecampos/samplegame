package com.samplegame.game.physic.type;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import lombok.Getter;

import static com.samplegame.game.GameScreen.world;

@Getter
public abstract class BodyModel {

    private FixtureDef fixtureDef = new FixtureDef();

    private BodyDef bodyDef = new BodyDef();

    BodyModel(boolean sensor, boolean fixedRotation, Float density, Float friction, Float restitution, Short category, Short maskBits) {
        this.fixtureDef.isSensor = sensor;
        this.bodyDef.fixedRotation = fixedRotation;
        this.fixtureDef.density = density == null ? this.fixtureDef.density : density;
        this.fixtureDef.friction = friction == null ? this.fixtureDef.friction : friction;
        this.fixtureDef.restitution = restitution == null ? this.fixtureDef.restitution : restitution;
        this.fixtureDef.filter.categoryBits = category == null ? this.fixtureDef.filter.categoryBits : category;
        this.fixtureDef.filter.maskBits = maskBits == null ? this.fixtureDef.filter.maskBits : maskBits;
    }

    public abstract Body build(int w, int h, float x, float y);

    public abstract Body build(Body body, int w, int h);

    Body buildBody() {
        return world.createBody(getBodyDef());
    }

    Body attachFixture(Body body, Shape shape) {
        getFixtureDef().shape = shape;
        body.createFixture(getFixtureDef());
        shape.dispose();
        return body;
    }

}
