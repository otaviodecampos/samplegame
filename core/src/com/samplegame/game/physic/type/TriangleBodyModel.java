package com.samplegame.game.physic.type;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class TriangleBodyModel extends BodyModel {

    public TriangleBodyModel(boolean sensor, boolean fixedRotation, Float density, Float friction, Float restitution, Short category, Short maskBits) {
        super(sensor, fixedRotation, density, friction, restitution, category, maskBits);
    }

    @Override
    public Body build(int w, int h, float x, float y) {
        getBodyDef().type = DynamicBody;
        getBodyDef().position.set(x + meter(half(w)), y + meter(half(h)));
        return build(buildBody(), w, h);
    }

    @Override
    public Body build(Body body, int w, int h) {
        float width = meter(w);
        float height = meter(h);
        attachFixture(body, buildTriangleShape(width, height));
        return body;
    }

    private Shape buildTriangleShape(float width, float height) {
        PolygonShape shape = new PolygonShape();
        Vector2[] vertices = new Vector2[3];
        vertices[0] = new Vector2(0.0f, -1.0f * half(height));
        vertices[1] = new Vector2(-1.0f * half(width), 1.0f * half(height));
        vertices[2] = new Vector2(1.0f * half(width), 1.0f * half(height));
        shape.set(vertices);
        return shape;
    }

}
