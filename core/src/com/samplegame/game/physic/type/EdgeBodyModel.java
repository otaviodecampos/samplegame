package com.samplegame.game.physic.type;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.Shape;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.StaticBody;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class EdgeBodyModel extends BodyModel {

    public EdgeBodyModel(boolean sensor, boolean fixedRotation, Float density, Float friction, Float restitution, Short category, Short maskBits) {
        super(sensor, fixedRotation, density, friction, restitution, category, maskBits);
    }

    @Override
    public Body build(int w, int h, float x, float y) {
        getBodyDef().type = StaticBody;
        getBodyDef().position.set(x + meter(half(w)), y + meter(half(h)));
        return build(buildBody(), w - 4, h - 4);
    }

    @Override
    public Body build(Body body, int w, int h) {
        float width = meter(w);
        float height = meter(h);
        attachFixture(body, buildEdgeShape(-width, height, width, height));
        attachFixture(body, buildEdgeShape(-width, height, -width, -height));
        attachFixture(body, buildEdgeShape(width, height, width, -height));
        attachFixture(body, buildEdgeShape(-width, -height, width, -height));
        return body;
    }

    private Shape buildEdgeShape(float x1, float y1, float x2, float y2) {
        EdgeShape shape = new EdgeShape();
        shape.set(half(x1), half(y1), half(x2), half(y2));
        return shape;
    }

}
