package com.samplegame.game.physic.type;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;

import static com.badlogic.gdx.physics.box2d.BodyDef.BodyType.DynamicBody;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class BoxBodyModel extends BodyModel {

    public BoxBodyModel(boolean sensor, boolean fixedRotation, Float density, Float friction, Float restitution, Short category, Short maskBits) {
        super(sensor, fixedRotation, density, friction, restitution, category, maskBits);
    }

    @Override
    public Body build(int w, int h, float x, float y) {
        getBodyDef().type = DynamicBody;
        getBodyDef().position.set(x + meter(half(w)), y + meter(half(h)));
        return build(buildBody(), w, h);
    }

    @Override
    public Body build(Body body, int w, int h) {
        float width = meter(w);
        float height = meter(h);
        attachFixture(body, buildBoxShape(width, height));
        return body;
    }

    private Shape buildBoxShape(float width, float height) {
        PolygonShape shape = new PolygonShape();
        shape.setAsBox(half(width), half(height));
        return shape;
    }

}
