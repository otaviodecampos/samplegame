package com.samplegame.game.physic.collision;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.samplegame.game.basic.component.DisposableComponent;
import com.samplegame.game.item.component.ItemComponent;
import com.samplegame.game.player.component.PlayerComponent;
import com.samplegame.game.ui.system.ItemBarSystem;

import static com.samplegame.game.GameScreen.engine;

public class CollisionContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {
        Entity entityA = (Entity) contact.getFixtureA().getBody().getUserData();
        Entity entityB = (Entity) contact.getFixtureB().getBody().getUserData();
        if (entityA != null && entityB != null) {
            if (entityA.getComponent(PlayerComponent.class) != null && entityB.getComponent(ItemComponent.class) != null) {
                playerItemContact(entityA, entityB);
            } else if (entityB.getComponent(PlayerComponent.class) != null && entityA.getComponent(ItemComponent.class) != null) {
                playerItemContact(entityB, entityA);
            }
        }
    }

    @Override
    public void endContact(Contact contact) {

    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    private void playerItemContact(Entity playerEntity, Entity itemEntity) {
        PlayerComponent player = playerEntity.getComponent(PlayerComponent.class);
        ItemComponent item = itemEntity.getComponent(ItemComponent.class);
        if (player.addItem(item.getType())) {
            itemEntity.add(engine.createComponent(DisposableComponent.class));
            engine.getSystem(ItemBarSystem.class).setProcessing(true);
        }
    }

}
