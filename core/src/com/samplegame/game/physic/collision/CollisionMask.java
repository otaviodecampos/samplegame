package com.samplegame.game.physic.collision;

public class CollisionMask {

    public static final short PLAYER_MASK = CollisionCategory.GROUND_CATEGORY;

    public static final short ITEM_MASK = CollisionCategory.GROUND_CATEGORY;

    public static final short GROUND_MASK = -1;

}
