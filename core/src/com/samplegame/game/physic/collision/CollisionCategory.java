package com.samplegame.game.physic.collision;

public class CollisionCategory {

    public static final short PLAYER_CATEGORY = 0x0001;

    public static final short ITEM_CATEGORY = 0x0002;

    public static final short GROUND_CATEGORY = 0x0004;

}
