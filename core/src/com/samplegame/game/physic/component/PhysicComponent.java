package com.samplegame.game.physic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.physics.box2d.Body;
import com.samplegame.game.GameScreen;
import com.samplegame.game.basic.component.ClearableComponent;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class PhysicComponent implements Component, ClearableComponent {

    private Body body;

    @Override
    public void clear() {
        GameScreen.world.destroyBody(body);
        body = null;
    }

}
