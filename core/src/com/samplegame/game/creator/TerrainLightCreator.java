package com.samplegame.game.creator;

import com.samplegame.game.terrain.component.TerrainLightComponent;

import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.createEntity;

public class TerrainLightCreator {

    public static void create() {
        engine.addEntity(createEntity(engine, TerrainLightComponent.class));
    }

}
