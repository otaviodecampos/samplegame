package com.samplegame.game.creator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.physics.box2d.Body;
import com.samplegame.game.basic.component.AnimationComponent;
import com.samplegame.game.basic.component.StateComponent;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.item.type.Item;
import com.samplegame.game.physic.collision.CollisionCategory;
import com.samplegame.game.physic.collision.CollisionMask;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.physic.type.BoxBodyModel;
import com.samplegame.game.player.component.PlayerComponent;

import java.util.stream.IntStream;

import static com.samplegame.GameConstants.*;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.createEntityWith;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class PlayerCreator {


    public static void create() {
        Entity entity = createEntityWith(engine, PlayerComponent.class, TextureComponent.class, TransformComponent.class, AnimationComponent.class, StateComponent.class, PhysicComponent.class);

        BoxBodyModel playerBodyModel = new BoxBodyModel(false, true, 1f, 0f, 0f, CollisionCategory.PLAYER_CATEGORY, CollisionMask.PLAYER_MASK);
        BoxBodyModel playerItemSensorBodyModel = new BoxBodyModel(true, true, null, null, null, CollisionCategory.GROUND_CATEGORY, CollisionMask.GROUND_MASK);

        PhysicComponent physic = entity.getComponent(PhysicComponent.class);
        Body body = playerBodyModel.build(PLAYER_WIDTH, PLAYER_HEIGHT, meter((WORLD_SIZE.x / 2) * TERRAIN_BLOCK_SIZE), meter(WORLD_INITIAL_PLAYER_ROW * TERRAIN_BLOCK_SIZE));
        playerItemSensorBodyModel.build(body, PLAYER_WIDTH, PLAYER_HEIGHT);
        body.setUserData(entity);
        physic.setBody(body);

        AnimationComponent animation = entity.getComponent(AnimationComponent.class);
        TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("player/assets.atlas"));
        Animation idleAnimation = new Animation<>(0.3f, atlas.findRegions("player/iddle"), Animation.PlayMode.LOOP);
        idleAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animation.getAnimations().put(PLAYER_STATE_IDLE, idleAnimation);

        Animation runAnimation = new Animation<>(0.15f, atlas.findRegions("player/run"), Animation.PlayMode.LOOP);
        runAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animation.getAnimations().put(PLAYER_STATE_RUN, runAnimation);

        Animation jumpAnimation = new Animation<>(0.15f, atlas.findRegions("player/jump"), Animation.PlayMode.LOOP);
        runAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animation.getAnimations().put(PLAYER_STATE_JUMP, jumpAnimation);

        Animation fallAnimation = new Animation<>(0.15f, atlas.findRegions("player/fall"), Animation.PlayMode.LOOP);
        runAnimation.setPlayMode(Animation.PlayMode.LOOP);
        animation.getAnimations().put(PLAYER_STATE_FALL, fallAnimation);

        TransformComponent transform = entity.getComponent(TransformComponent.class);
        transform.setWidth(PLAYER_WIDTH);
        transform.setHeight(PLAYER_TEXTURE_HEIGHT);
        transform.setIndex(5);

        PlayerComponent player = entity.getComponent(PlayerComponent.class);
        IntStream.range(0, 99).forEach(value -> player.addItem(Item.TORCH));

        engine.addEntity(entity);
    }

}
