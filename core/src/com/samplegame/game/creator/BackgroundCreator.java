package com.samplegame.game.creator;

import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.samplegame.GameConstants;
import com.samplegame.game.background.component.BackgroundComponent;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.utility.CacheUtility;

import static com.badlogic.gdx.graphics.Texture.TextureWrap.MirroredRepeat;
import static com.badlogic.gdx.graphics.Texture.TextureWrap.Repeat;
import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.createEntityWith;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;

public class BackgroundCreator {

    public static void create() {
        TextureRegion textureRegion = CacheUtility.getTextureRegion("background/sky.gif");
        textureRegion.setRegion(0, 0, (int) GameConstants.WORLD_SIZE.x * TERRAIN_BLOCK_SIZE, textureRegion.getTexture().getHeight());
        textureRegion.getTexture().setWrap(Repeat, MirroredRepeat);

        int backgroundWidth = (int) GameConstants.WORLD_SIZE.x * TERRAIN_BLOCK_SIZE;
        int backgroundHeight = textureRegion.getTexture().getHeight();

        Entity entity = createEntityWith(engine, BackgroundComponent.class, TransformComponent.class, TextureComponent.class);
        TextureComponent texture = entity.getComponent(TextureComponent.class);
        texture.setRegion(textureRegion);

        TransformComponent transform = entity.getComponent(TransformComponent.class);
        transform.setWidth(backgroundWidth);
        transform.setHeight(backgroundHeight);
        transform.getPosition().set(meter(backgroundWidth - half(backgroundWidth)), meter(backgroundHeight - half(backgroundHeight) + TERRAIN_BLOCK_SIZE));
        transform.setIndex(-9);

        engine.addEntity(entity);

        backgroundHeight = (int) GameConstants.WORLD_SIZE.y * TERRAIN_BLOCK_SIZE + TERRAIN_BLOCK_SIZE;

        entity = createEntityWith(engine, BackgroundComponent.class, TransformComponent.class, TextureComponent.class);
        texture = entity.getComponent(TextureComponent.class);
        textureRegion = CacheUtility.getTextureRegion("background/block.png");
        textureRegion.setRegion(0, 0, backgroundWidth, backgroundHeight);
        textureRegion.getTexture().setWrap(Repeat, Repeat);
        texture.setRegion(textureRegion);

        transform = entity.getComponent(TransformComponent.class);
        transform.setWidth(backgroundWidth);
        transform.setHeight(backgroundHeight);
        transform.getPosition().set(meter(backgroundWidth - half(backgroundWidth)), meter(-half(backgroundHeight) + TERRAIN_BLOCK_SIZE));
        transform.setIndex(-10);

        engine.addEntity(entity);
    }

}
