package com.samplegame.game.creator;

import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.terrain.component.TerrainComponent;
import com.samplegame.game.terrain.builder.TerrainBuilder;

import static com.samplegame.GameConstants.WORLD_SIZE;
import static com.samplegame.game.GameScreen.engine;

public class TerrainCreator {

    public static void create() {
        TerrainComponent terrain = engine.createComponent(TerrainComponent.class);
        TerrainBuilder terrainBuilder = new TerrainBuilder();
        terrainBuilder.build(WORLD_SIZE).forEach(terrainBlock -> {
            terrain.add(terrainBlock, false);
        });
        EntityUtility.createAddEntity(engine, terrain);
    }

}
