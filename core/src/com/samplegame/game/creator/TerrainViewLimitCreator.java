package com.samplegame.game.creator;

import com.samplegame.game.terrain.component.TerrainViewLimitComponent;

import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.utility.EntityUtility.createEntity;

public class TerrainViewLimitCreator {

    public static void create() {
        engine.addEntity(createEntity(engine, TerrainViewLimitComponent.class));
    }

}
