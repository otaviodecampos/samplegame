package com.samplegame.game.ui.listener;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.samplegame.game.ui.system.ItemBarSystem;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ItemBoxHoverListener extends InputListener {

    private ItemBarSystem itemBarSystem;

    @Override
    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
        itemBarSystem.setMouseHover(true);
        itemBarSystem.setHoverItemBox((int) event.getListenerActor().getUserObject());
    }

    @Override
    public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
        if (pointer == -1) {
            itemBarSystem.setMouseHover(false);
            itemBarSystem.setHoverItemBox(-1);
        }
    }

}
