package com.samplegame.game.ui.listener;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.samplegame.game.ui.system.ItemBarSystem;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import static com.samplegame.game.ui.constants.ItemBarConstants.ITEM_BAR_ITEM_SIZE;

@RequiredArgsConstructor
public class ItemBoxDragListener extends DragListener {

    @NonNull
    private ItemBarSystem itemBarSystem;

    @NonNull
    private Group itemGroup;

    private Vector2 origin;

    @Override
    public void dragStart(InputEvent event, float x, float y, int pointer) {
        origin = new Vector2(itemGroup.getX(), itemGroup.getY());
        itemBarSystem.setMouseDrag(true);
        itemBarSystem.getItemBar().getChildren().get(itemBarSystem.getItemBar().getChildren().indexOf(event.getListenerActor().getParent(), true)).toFront();
    }

    @Override
    public void drag(InputEvent event, float x, float y, int pointer) {
        itemGroup.moveBy(x - ITEM_BAR_ITEM_SIZE, y);
    }

    @Override
    public void dragStop(InputEvent event, float x, float y, int pointer) {
        itemGroup.setX(origin.x);
        itemGroup.setY(origin.y);
        itemBarSystem.setMouseDrag(false);

        int originItemBoxIndex = (int) itemGroup.getParent().getUserObject();
        int targetItemBoxIndex = itemBarSystem.getHoverItemBox();
        if (targetItemBoxIndex == -1) {
            itemBarSystem.dropItem(originItemBoxIndex);
        } else {
            itemBarSystem.swapItem(originItemBoxIndex, targetItemBoxIndex);
        }
    }
}
