package com.samplegame.game.ui.listener;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.SnapshotArray;
import com.samplegame.game.ui.system.ItemBarSystem;
import com.samplegame.game.ui.type.ImageShader;
import lombok.AllArgsConstructor;

import static com.samplegame.game.basic.shader.GrayscaleShader.GRAYSCALE_SHADER;
import static com.samplegame.game.ui.constants.ItemBarConstants.ITEM_BAR_BOX_IMAGE_NAME;

@AllArgsConstructor
public class ItemBoxClickListener extends ClickListener {

    private ItemBarSystem itemBarSystem;

    @Override
    public void clicked(InputEvent event, float x, float y) {
        int itemBoxIndex = (int) event.getListenerActor().getUserObject();
        SnapshotArray<Actor> itemBackgroundBoxes = itemBarSystem.getItemBarBackground().getChildren();
        setActive((Group) itemBackgroundBoxes.get(itemBarSystem.getSelectedItemBox()), false);
        setActive((Group) itemBackgroundBoxes.get(itemBoxIndex), true);
        itemBarSystem.setSelectedItemBox(itemBoxIndex);
    }

    private void setActive(Group itemBox, boolean active) {
        ImageShader imageShader = itemBox.findActor(ITEM_BAR_BOX_IMAGE_NAME);
        imageShader.setShader(active ? null : GRAYSCALE_SHADER);
    }

}
