package com.samplegame.game.ui.type;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageShader extends Image {

    private ShaderProgram shader;

    public ImageShader(TextureRegion region, ShaderProgram shader) {
        super(region);
        this.shader = shader;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setShader(shader);
        super.draw(batch, parentAlpha);
        batch.setShader(null);
    }
}
