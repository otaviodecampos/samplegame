package com.samplegame.game.ui.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.scenes.scene2d.Actor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UiComponent implements Component {

    private Actor actor;

    public UiComponent setActor(Actor actor) {
        this.actor = actor;
        return this;
    }

}
