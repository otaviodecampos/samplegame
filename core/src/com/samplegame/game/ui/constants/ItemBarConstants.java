package com.samplegame.game.ui.constants;

public class ItemBarConstants {

    public static final String ITEM_BAR_BOX_IMAGE_TEXTURE_PATH = "ui/item_box.png";

    public static final String ITEM_BAR_BOX_IMAGE_NAME = "boxImage";

    public static final int ITEM_BAR_BOX_SIZE = 48;

    public static final String ITEM_BAR_ITEM_GROUP_NAME = "itemGroup";

    public static final int ITEM_BAR_ITEM_SIZE = 24;

    public static final int ITEM_BAR_ITEM_LABEL_FONT_SIZE = 16;

    public static final String ITEM_BAR_ITEM_LABEL_FONT_PATH = "fonts/DisposableDroidBB_bld.ttf";

    public static final String ITEM_BAR_ITEM_LABEL_NAME = "label";

}
