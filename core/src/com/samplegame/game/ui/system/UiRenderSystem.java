package com.samplegame.game.ui.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.ui.component.UiComponent;

public class UiRenderSystem extends EntitySystem {

    private Stage stage;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);

        stage = new Stage(new ScreenViewport(), new SpriteBatch());
        Gdx.input.setInputProcessor(stage);

        for (Entity entity : EntityUtility.findEntities(getEngine(), UiComponent.class)) {
            stage.addActor(entity.getComponent(UiComponent.class).getActor());
        }
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        stage.act(deltaTime);
        stage.draw();
    }

    public void resize() {
        stage.getViewport().update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
    }

}
