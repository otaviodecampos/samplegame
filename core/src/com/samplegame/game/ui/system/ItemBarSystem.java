package com.samplegame.game.ui.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.type.Random;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.item.component.ItemComponent;
import com.samplegame.game.item.type.Item;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.player.component.PlayerComponent;
import com.samplegame.game.player.type.ItemInfo;
import com.samplegame.game.ui.component.UiComponent;
import com.samplegame.game.ui.listener.ItemBoxClickListener;
import com.samplegame.game.ui.listener.ItemBoxDragListener;
import com.samplegame.game.ui.listener.ItemBoxHoverListener;
import com.samplegame.game.ui.type.ImageShader;
import lombok.Getter;
import lombok.Setter;

import static com.samplegame.GameConstants.PLAYER_MAX_ITEMS;
import static com.samplegame.GameConstants.PLAYER_WIDTH;
import static com.samplegame.game.GameScreen.engine;
import static com.samplegame.game.basic.shader.GrayscaleShader.GRAYSCALE_SHADER;
import static com.samplegame.game.basic.utility.CacheUtility.getFont;
import static com.samplegame.game.basic.utility.CacheUtility.getTextureRegion;
import static com.samplegame.game.basic.utility.EntityUtility.*;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;
import static com.samplegame.game.basic.utility.NumberUtility.invert;
import static com.samplegame.game.ui.constants.ItemBarConstants.*;

public class ItemBarSystem extends EntitySystem {

    @Getter
    private Table itemBarBackground;

    @Getter
    private Table itemBar;

    private PlayerComponent player;

    private Label.LabelStyle labelStyle;

    @Getter
    @Setter
    private boolean mouseHover;

    @Getter
    @Setter
    private boolean mouseDrag;

    @Getter
    @Setter
    private int selectedItemBox;

    @Getter
    @Setter
    private int hoverItemBox;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        player = EntityUtility.findComponent(getEngine(), PlayerComponent.class);
        labelStyle = createLabelStyle();
        createAddEntity(getEngine(), createItemBarBackground());
        createAddEntity(getEngine(), createItemBar());
    }

    public void update(float deltaTime) {
        for (Actor actor : itemBar.getChildren()) {
            Group itemBox = (Group) actor;
            itemBox.removeActor(itemBox.findActor(ITEM_BAR_ITEM_GROUP_NAME));

            Integer index = (Integer) itemBox.getUserObject();
            if (index == null) {
                index = itemBar.getChildren().indexOf(actor, true);
            }
            ItemInfo item = player.getItem(index);

            if (item != null) {
                Group itemGroup = createItemGroup(itemBox, item);
                itemGroup.setZIndex(index == selectedItemBox ? 1 : 0);
                itemGroup.addListener(new ItemBoxDragListener(this, itemGroup));
                itemBox.addActor(itemGroup);
            }
        }
        setProcessing(false);
    }

    private UiComponent createItemBarBackground() {
        itemBarBackground = new Table();
        itemBarBackground.setSize(ITEM_BAR_BOX_SIZE * PLAYER_MAX_ITEMS, ITEM_BAR_BOX_SIZE);
        itemBarBackground.setPosition(0, 0);
        itemBarBackground.setFillParent(true);
        itemBarBackground.top();
        itemBarBackground.padTop(16);

        for (int i = 0; i < PLAYER_MAX_ITEMS; i++) {
            Group itemBox = createItemBox(true, i == selectedItemBox);
            itemBarBackground.add(itemBox).pad(-1);
        }

        return createComponent(getEngine(), UiComponent.class).setActor(itemBarBackground);
    }

    private UiComponent createItemBar() {
        itemBar = new Table();
        itemBar.setSize(ITEM_BAR_BOX_SIZE * PLAYER_MAX_ITEMS, ITEM_BAR_BOX_SIZE);
        itemBar.setPosition(0, 0);
        itemBar.setFillParent(true);
        itemBar.top();
        itemBar.padTop(16);

        ItemBoxClickListener clickListener = new ItemBoxClickListener(this);
        ItemBoxHoverListener hoverListener = new ItemBoxHoverListener(this);
        for (int i = 0; i < PLAYER_MAX_ITEMS; i++) {
            Group itemBox = createItemBox(false, i == selectedItemBox);
            itemBox.setUserObject(i);
            itemBox.addListener(clickListener);
            itemBox.addListener(hoverListener);
            itemBar.add(itemBox).pad(-1);
        }

        return createComponent(getEngine(), UiComponent.class).setActor(itemBar);
    }

    private Group createItemBox(boolean setBackground, boolean selected) {
        Group group = new Group();
        group.setSize(ITEM_BAR_BOX_SIZE, ITEM_BAR_BOX_SIZE);
        if (setBackground) {
            ImageShader image = new ImageShader(getTextureRegion(ITEM_BAR_BOX_IMAGE_TEXTURE_PATH), selected ? null : GRAYSCALE_SHADER);
            image.setName(ITEM_BAR_BOX_IMAGE_NAME);
            group.addActor(image);
        }
        return group;
    }

    private Group createItemGroup(Group box, ItemInfo item) {
        Group itemGroup = new Group();
        itemGroup.setName(ITEM_BAR_ITEM_GROUP_NAME);

        Image image = createItemIcon(item.getItem());
        image.setPosition(box.getWidth() / 2f - image.getWidth() / 2f, box.getHeight() / 2f - image.getHeight() / 2f);
        itemGroup.addActor(image);

        Label label = new Label(String.valueOf(item.getQuantity()), labelStyle);
        label.setName(ITEM_BAR_ITEM_LABEL_NAME);
        label.setWidth(ITEM_BAR_BOX_SIZE);
        label.setHeight(ITEM_BAR_BOX_SIZE);
        label.setAlignment(Align.bottomRight);
        label.setPosition(-8, 6);
        itemGroup.addActor(label);

        return itemGroup;
    }

    private Image createItemIcon(Item itemType) {
        Image image = new Image(getTextureRegion(itemType.getTexturePath()));
        image.setSize(ITEM_BAR_ITEM_SIZE, ITEM_BAR_ITEM_SIZE);
        return image;
    }

    private Label.LabelStyle createLabelStyle() {
        Label.LabelStyle labelStyle = new Label.LabelStyle();
        labelStyle.fontColor = Color.WHITE;
        labelStyle.font = getFont(ITEM_BAR_ITEM_LABEL_FONT_PATH, labelStyle.fontColor, ITEM_BAR_ITEM_LABEL_FONT_SIZE);
        return labelStyle;
    }

    public void swapItem(int originItemBoxIndex, int targetItemBoxIndex) {
        player.swapItem(originItemBoxIndex, targetItemBoxIndex);
        update(0);
    }

    public void dropItem(int originItemBoxIndex) {
        ItemInfo item = player.removeItem(originItemBoxIndex);
        createDropItemEntities(item).forEach(getEngine()::addEntity);
        update(0);
    }

    private Array<Entity> createDropItemEntities(ItemInfo itemInfo) {
        Array<Entity> entities = new Array<>();
        for (int i = 0; i < itemInfo.getQuantity(); i++) {
            TransformComponent playerTransform = findEntity(getEngine(), PlayerComponent.class).getComponent(TransformComponent.class);
            float dropDistance = invert(PLAYER_WIDTH, playerTransform.getScaleDirection());
            float forceX = invert(Random.next(0f, 3.00f), playerTransform.getScaleDirection());
            float forceY = Random.next(0f, 2.00f);

            ItemComponent item = engine.createComponent(ItemComponent.class);
            Item itemType = itemInfo.getItem();
            item.setType(itemType);

            TransformComponent transform = engine.createComponent(TransformComponent.class);
            transform.getPosition().set(playerTransform.getPosition());
            transform.setWidth((int) half(itemType.getWidth()));
            transform.setHeight((int) half(itemType.getHeight()));

            TextureComponent texture = engine.createComponent(TextureComponent.class);
            texture.setRegion(getTextureRegion(itemType.getTexturePath()));

            PhysicComponent physic = engine.createComponent(PhysicComponent.class);
            Body body = item.getType().getBodyModel().build(transform.getWidth(), transform.getHeight(), playerTransform.getPosition().x + meter(dropDistance), playerTransform.getPosition().y);
            physic.setBody(body);
            body.applyForce(forceX, forceY, body.getPosition().x, body.getPosition().y, true);

            Entity entity = createEntity(engine, item, transform, texture, physic);
            body.setUserData(entity);
            entities.add(entity);
        }
        return entities;
    }

}
