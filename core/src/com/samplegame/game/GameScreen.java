package com.samplegame.game;

import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.samplegame.SampleGame;
import com.samplegame.game.basic.system.*;
import com.samplegame.game.creator.*;
import com.samplegame.game.item.system.TerrainItemActionSystem;
import com.samplegame.game.physic.collision.CollisionContactListener;
import com.samplegame.game.physic.system.PhysicSystem;
import com.samplegame.game.player.system.PlayerInputSystem;
import com.samplegame.game.terrain.component.TerrainItemComponent;
import com.samplegame.game.terrain.listener.TerrainItemRemoveListener;
import com.samplegame.game.terrain.system.*;
import com.samplegame.game.terrain.system.item.TerrainItemTorchSystem;
import com.samplegame.game.ui.system.ItemBarSystem;
import com.samplegame.game.ui.system.UiRenderSystem;

/**
 * Created by otavio on 12/03/2017.
 */
public class GameScreen implements Screen {

    public static PooledEngine engine;

    public static World world;

    public GameScreen() {
        engine = new PooledEngine();
        world = new World(new Vector2(0, -10f), true);

        BackgroundCreator.create();
        TerrainCreator.create();
        TerrainViewLimitCreator.create();
        TerrainLightCreator.create();
        PlayerCreator.create();

        engine.addEntityListener(Family.all(TerrainItemComponent.class).get(), new TerrainItemRemoveListener());
        world.setContactListener(new CollisionContactListener());

        engine.addSystem(new ItemBarSystem());
        engine.addSystem(new PhysicSystem());
        engine.addSystem(new PlayerInputSystem());
        engine.addSystem(new GameCameraSystem());
        engine.addSystem(new AnimationSystem());
        engine.addSystem(new StateSystem());
        engine.addSystem(new RotationSystem());
        engine.addSystem(new TerrainItemActionSystem());
        engine.addSystem(new TerrainViewLimitSystem());
        engine.addSystem(new TerrainLightSystem());
        engine.addSystem(new TerrainSystem());
        engine.addSystem(new TerrainItemTorchSystem());
        engine.addSystem(new TerrainItemInputSystem());
        engine.addSystem(new TerrainItemHighlightSystem());
        engine.addSystem(new TerrainItemHitSystem());
        engine.addSystem(new TextureRenderSystem());
        engine.addSystem(new RectangleRenderSystem());
        engine.addSystem(new LineRenderSystem());
        engine.addSystem(new TerrainItemRefreshSystem());
        engine.addSystem(new TerrainItemVisibleSystem());
        engine.addSystem(new UiRenderSystem());
        engine.addSystem(new DisposableSystem());

        if (SampleGame.debug) {
            engine.addSystem(new DebugRenderSystem());
        }
    }

    @Override
    public void show() {
    }

    @Override
    public void render(float delta) {
        world.step(1f / 60f, 6, 2);
        engine.update(delta);
        Gdx.graphics.setTitle(String.valueOf(Gdx.graphics.getFramesPerSecond()));
    }

    @Override
    public void resize(int width, int height) {
        engine.getSystem(GameCameraSystem.class).resize();
        engine.getSystem(UiRenderSystem.class).resize();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        engine.getSystem(TextureRenderSystem.class).dispose();
    }

}
