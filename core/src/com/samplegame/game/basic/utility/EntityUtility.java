package com.samplegame.game.basic.utility;

import com.badlogic.ashley.core.*;
import com.badlogic.ashley.utils.ImmutableArray;

import static com.badlogic.ashley.core.ComponentMapper.getFor;

/**
 * Created by otavio on 14/03/2017.
 */
public class EntityUtility {

    @SafeVarargs
    public static <EngineType extends Engine> Entity createEntityWith(EngineType engine, Class<? extends Component>... componentClasses) {
        PooledEngine pooledEngine = ((PooledEngine) engine);
        Entity entity = pooledEngine.createEntity();

        for (Class<? extends Component> componentClass : componentClasses) {
            entity.add(pooledEngine.createComponent(componentClass));
        }

        return entity;
    }

    public static <EngineType extends Engine> Entity createEntity(EngineType engine, Component... components) {
        return createEntity(engine, null, components);
    }

    public static <EngineType extends Engine> Entity createEntity(EngineType engine, Class<? extends Component> componentClass, Component... components) {
        PooledEngine pooledEngine = ((PooledEngine) engine);
        Entity entity = pooledEngine.createEntity();

        if (componentClass != null) {
            entity.add(pooledEngine.createComponent(componentClass));
        }

        for (Component component : components) {
            entity.add(component);
        }

        return entity;
    }

    public static <EngineType extends Engine> Entity createAddEntity(EngineType engine, Component... components) {
        Entity entity = createEntity(engine, components);
        engine.addEntity(entity);
        return entity;
    }

    public static <EngineType extends Engine, ComponentType extends Component> ComponentType createComponent(EngineType engine, Class<ComponentType> componentClass) {
        return ((PooledEngine) engine).createComponent(componentClass);
    }

    public static <EngineType extends Engine, ComponentType extends Component> ComponentType findComponent(EngineType engine, Class<ComponentType> componentClass) {
        return findEntity(engine, componentClass).getComponent(componentClass);
    }

    @SafeVarargs
    public static <EngineType extends Engine, ComponentType extends Component> ImmutableArray<Entity> findEntities(EngineType engine, Class<ComponentType>... componentClasses) {
        return engine.getEntitiesFor(Family.all(componentClasses).get());
    }

    @SafeVarargs
    public static <EngineType extends Engine, ComponentType extends Component> Entity findEntity(EngineType engine, Class<ComponentType>... componentClasses) {
        return engine.getEntitiesFor(Family.all(componentClasses).get()).first();
    }

    public static <ComponentType extends Component> ComponentType getComponent(Entity entity, Class<ComponentType> componentClass) {
        return getFor(componentClass).get(entity);
    }

}
