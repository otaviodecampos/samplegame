package com.samplegame.game.basic.utility;

public class MeasureUtility {

    public static final float PIXELS_BY_METER = 128f;

    public static float meter(float pixel) {
        return pixel / PIXELS_BY_METER;
    }

    public static float pixel(float meter) {
        return meter * PIXELS_BY_METER;
    }

    public static float half(float measure) {
        return measure * 0.5f;
    }

}
