package com.samplegame.game.basic.utility;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.samplegame.game.basic.component.LineComponent;
import com.samplegame.game.basic.component.TransformComponent;

import java.util.Comparator;

public class LineLayerComparator implements Comparator<Entity> {

    private ComponentMapper<LineComponent> pm = ComponentMapper.getFor(LineComponent.class);

    @Override
    public int compare(Entity e1, Entity e2) {
        return (int) Math.signum(pm.get(e1).getLayer() - pm.get(e2).getLayer());
    }

}
