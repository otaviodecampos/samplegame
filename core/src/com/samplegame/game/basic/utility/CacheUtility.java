package com.samplegame.game.basic.utility;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.badlogic.gdx.utils.ObjectMap;

/**
 * Created by otavio on 05/03/2017.
 */
public class CacheUtility {

    private static ObjectMap<String, Texture> textures = new ObjectMap<>();

    private static ObjectMap<String, TextureRegion> texturesRegion = new ObjectMap<>();

    private static ObjectMap<String, TextureAtlas> texturesAtlas = new ObjectMap<>();

    private static ObjectMap<String, BitmapFont> fonts = new ObjectMap<>();

    private static ObjectMap<String, TextureRegion> highlightTextures = new ObjectMap<>();

    private static TextureAtlas atlas = new TextureAtlas(Gdx.files.internal("player/assets.atlas"));

    private static ObjectMap<String, Array<TextureAtlas.AtlasRegion>> atlasRegions = new ObjectMap<>();

    private static ObjectMap<String, ParticleEffect> particleEffects = new ObjectMap<>();

    public static ParticleEffect getParticleEffect(String particlePath, String imageDirPath) {
        ParticleEffect particleEffect = particleEffects.get(particlePath);
        if(particleEffect == null) {
            particleEffect = new ParticleEffect();
            particleEffect.load(Gdx.files.internal(particlePath), Gdx.files.internal(imageDirPath));
        }
        return new ParticleEffect(particleEffect);
    }

    private static TextureAtlas.AtlasRegion findAtlasRegion(String namePath, int index) {
        Array<TextureAtlas.AtlasRegion> regions = atlasRegions.get(namePath);
        if (regions == null) {
            regions = atlas.findRegions(namePath);
            atlasRegions.put(namePath, regions);
        }
        return regions.get(0);
    }

    private static Texture getTexture(String path) {
        Texture texture = textures.get(path);
        if (texture == null) {
            try {
                texture = new Texture(path);
                textures.put(path, texture);
            } catch (GdxRuntimeException e) {
                // ...
            }
        }
        return texture;
    }

    public static TextureRegion getTextureRegion(String path) {
        TextureRegion textureRegion = texturesRegion.get(path);
        if (textureRegion == null) {
            Texture texture = CacheUtility.getTexture(path);
            if (texture == null) {
                textureRegion = findAtlasRegion(path, 0);
            } else {
                textureRegion = new TextureRegion(texture);
            }
            texturesRegion.put(path, textureRegion);
        }
        return textureRegion;
    }

    public static TextureAtlas getTextureAtlas(String path) {
        TextureAtlas textureAtlas = texturesAtlas.get(path);
        if (textureAtlas == null) {
            textureAtlas = new TextureAtlas(path);
            texturesAtlas.put(path, textureAtlas);
        }
        return textureAtlas;
    }

    public static BitmapFont getFont(String file, Color color, int size) {
        String key = file + ":" + color + ":" + size;
        BitmapFont font = fonts.get(key);
        if (font == null) {
            FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal(file));
            FreeTypeFontGenerator.FreeTypeFontParameter fontParameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
            fontParameter.size = size;
            fontParameter.borderColor = Color.BLACK;
            fontParameter.borderWidth = 1;
            font = gen.generateFont(fontParameter);
            font.setColor(color);
            font.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
            fonts.put(key, font);
        }
        return font;
    }

    public static TextureRegion getHighlightTextureRegion(String path) {
        TextureRegion highlightTextureRegion = highlightTextures.get(path);
        if (highlightTextureRegion == null) {
            TextureAtlas.AtlasRegion originAtlasRegion = (TextureAtlas.AtlasRegion) getTextureRegion(path);
            TextureAtlas.AtlasRegion highlightAtlasRegion = new TextureAtlas.AtlasRegion(originAtlasRegion);
            highlightAtlasRegion.setTexture(buildHighlightTexture(originAtlasRegion.getTexture()));
            highlightTextures.put(path, highlightAtlasRegion);
        }
        return highlightTextureRegion;
    }

    private static Texture buildHighlightTexture(Texture texture) {
        if (!texture.getTextureData().isPrepared()) {
            texture.getTextureData().prepare();
        }
        Pixmap pixmap = texture.getTextureData().consumePixmap();
        Color color = new Color();
        for (int x = 0; x < pixmap.getWidth(); x++) {
            for (int y = 0; y < pixmap.getHeight(); y++) {
                int val = pixmap.getPixel(x, y);
                Color.rgba8888ToColor(color, val);
                int A = (int) (color.a * 255f);
                int R = (int) (color.r * 255f);
                int G = (int) (color.g * 255f);
                int B = (int) (color.b * 255f);
                pixmap.setColor(1f, 1f, 1f, .3f);
                if (A != 0 && R != 0 && G != 0 && B != 0) {
                    pixmap.drawPixel(x, y);
                }
            }
        }
        Texture highlightTexture = new Texture(pixmap);
        pixmap.dispose();
        return highlightTexture;
    }
}
