package com.samplegame.game.basic.utility;

import com.samplegame.game.basic.type.Direction;

public class NumberUtility {

    public static float invert(float number, Direction direction) {
        return invert(number, direction.getValue());
    }

    public static float invert(float number, int value) {
        return invert(number, value < 0);
    }

    public static float invert(float number, boolean invert) {
        if (invert) {
            number = -number;
        }
        return number;
    }

}
