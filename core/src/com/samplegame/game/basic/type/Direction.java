package com.samplegame.game.basic.type;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum Direction {

    NONE(0),
    LEFT(1),
    RIGHT(-1),
    TOP(10),
    BOTTOM(-10);

    private int value;

    public static Direction get(Number value) {
        return Arrays.stream(Direction.values()).filter(direction -> value.intValue() == direction.value).findFirst().orElse(NONE);
    }

}
