package com.samplegame.game.basic.type;

import java.util.SplittableRandom;

public class Random {

    public static int next(int start, int end) {
        return new SplittableRandom().nextInt(start, end);
    }

    public static float next(double start, double end) {
        return (float) new SplittableRandom().nextDouble(start, end);
    }

}
