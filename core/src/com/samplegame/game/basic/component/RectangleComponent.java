package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RectangleComponent implements Component, ClearableComponent {

    private Color fillColor;

    @Override
    public void clear() {
        fillColor = null;
    }
}
