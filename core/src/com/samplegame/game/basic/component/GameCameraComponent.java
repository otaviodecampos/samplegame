package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.graphics.OrthographicCamera;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class GameCameraComponent implements Component {

    private Entity target;

    private OrthographicCamera camera;

}
