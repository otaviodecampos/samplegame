package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpacityComponent implements Component {

    private float opacity;

}
