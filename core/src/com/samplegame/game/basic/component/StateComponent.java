package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StateComponent implements Component, ClearableComponent {

    private int current = 0;

    private float time = 0.0f;

    public void setState(int newState) {
        current = newState;
        time = 0.0f;
    }

    public void addTime(float deltaTime) {
        time += deltaTime;
    }

    @Override
    public void clear() {
        current = 0;
        time = 0.0f;
    }
}

