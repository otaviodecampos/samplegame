package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ColorComponent implements Component {

    private float red = 1f;

    private float green = 1f;

    private float blue = 1f;

    public void set(float color) {
        red = color;
        green = color;
        blue = color;
    }

}
