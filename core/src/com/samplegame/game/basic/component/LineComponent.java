package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LineComponent implements Component {

    private Color color;

    private Vector2 startPosition;

    private Vector2 endPosition;

    private int layer;

}
