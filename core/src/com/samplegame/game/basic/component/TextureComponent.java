package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TextureComponent implements Component, ClearableComponent {

    private TextureRegion region;

    @Override
    public void clear() {
        region = null;
    }
}
