package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.samplegame.game.basic.type.Direction;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RotationComponent implements Component, ClearableComponent {

    private Direction direction;

    private float frameDuration;

    @Override
    public void clear() {
        frameDuration = 0f;
        direction = null;
    }
}
