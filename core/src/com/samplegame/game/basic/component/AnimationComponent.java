package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.IntMap;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AnimationComponent implements Component, ClearableComponent {

    private IntMap<Animation> animations = new IntMap<>();

    @Override
    public void clear() {
        animations.clear();
    }
}
