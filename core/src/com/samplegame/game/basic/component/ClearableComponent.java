package com.samplegame.game.basic.component;

public interface ClearableComponent {

    void clear();

}
