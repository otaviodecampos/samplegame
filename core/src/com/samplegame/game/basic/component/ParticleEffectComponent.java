package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParticleEffectComponent implements Component {

    private ParticleEffect particleEffect;

}
