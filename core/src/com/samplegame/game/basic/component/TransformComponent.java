package com.samplegame.game.basic.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;
import com.samplegame.game.basic.type.Direction;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransformComponent implements Component, ClearableComponent {

    private int width;

    private int height;

    private int index;

    private float rotation = 0;

    private final Vector2 scale = new Vector2(1, 1);

    private final Vector2 position = new Vector2(0, 0);

    public Direction getScaleDirection() {
        return Direction.get(Math.signum(scale.x));
    }

    @Override
    public void clear() {
        width = 0;
        height = 0;
        index = 0;
        rotation = 0;
        scale.x = 1;
        scale.y = 1;
        position.x = 0;
        position.y = 0;
    }
}
