package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.samplegame.game.basic.component.RotationComponent;
import com.samplegame.game.basic.component.StateComponent;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;

public class RotationSystem extends IteratingSystem {

    public RotationSystem() {
        super(Family.all(TextureComponent.class, RotationComponent.class, TransformComponent.class, StateComponent.class).get());
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        StateComponent state = getComponent(entity, StateComponent.class);
        RotationComponent rotation = getComponent(entity, RotationComponent.class);
        TransformComponent transform = getComponent(entity, TransformComponent.class);
        transform.setRotation(transform.getRotation() + (state.getTime() * rotation.getDirection().getValue()) / rotation.getFrameDuration());
    }

}
