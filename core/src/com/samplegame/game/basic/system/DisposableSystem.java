package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Component;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.samplegame.game.basic.component.AnimationComponent;
import com.samplegame.game.basic.component.ClearableComponent;
import com.samplegame.game.basic.component.DisposableComponent;
import com.samplegame.game.basic.component.StateComponent;

/**
 * Created by otavio on 20/03/2017.
 */
public class DisposableSystem extends IteratingSystem {

    public DisposableSystem() {
        super(Family.all(DisposableComponent.class).get());
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        boolean discard = true;
        StateComponent state = entity.getComponent(StateComponent.class);
        AnimationComponent animation = entity.getComponent(AnimationComponent.class);

        if (state != null && animation != null) {
            discard = false;
            Animation keyFrame = animation.getAnimations().get(state.getCurrent());
            if (keyFrame.isAnimationFinished(state.getTime())) {
                discard = true;
            }
        }

        if (discard) {
            for (Component component : entity.getComponents()) {
                if (component instanceof ClearableComponent) {
                    ClearableComponent clearableComponent = (ClearableComponent) component;
                    clearableComponent.clear();
                }
            }
            entity.removeAll();
            getEngine().removeEntity(entity);
        }
    }
}
