package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.basic.component.GameCameraComponent;
import com.samplegame.game.basic.component.RectangleComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.utility.EntityUtility;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.pixel;

public class RectangleRenderSystem extends IteratingSystem {

    private GameCameraComponent viewport;

    private Array<Entity> renderQueue;

    private ShapeRenderer shapeRenderer;

    public RectangleRenderSystem() {
        super(Family.all(RectangleComponent.class, TransformComponent.class).get());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        viewport = EntityUtility.findComponent(engine, GameCameraComponent.class);
        renderQueue = new Array<>();
        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        for (Entity entity : renderQueue) {
            RectangleComponent shape = getComponent(entity, RectangleComponent.class);
            shapeRenderer.setColor(shape.getFillColor());

            TransformComponent transform = getComponent(entity, TransformComponent.class);
            float x = pixel(transform.getPosition().x) - half(transform.getWidth());
            float y = pixel(transform.getPosition().y) - half(transform.getHeight());

            shapeRenderer.rect(x, y,
                    half(transform.getWidth()), half(transform.getHeight()),
                    transform.getWidth(), transform.getHeight(),
                    transform.getScale().x, transform.getScale().y,
                    MathUtils.radiansToDegrees * transform.getRotation());
        }

        shapeRenderer.end();
        renderQueue.clear();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }
}
