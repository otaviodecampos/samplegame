package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.basic.component.*;
import com.samplegame.game.basic.utility.TextureLayerComparator;
import lombok.Getter;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.pixel;

/**
 * Created by otavio on 12/03/2017.
 */
public class TextureRenderSystem extends SortedIteratingSystem {

    @Getter
    private SpriteBatch batch;

    private GameCameraComponent viewport;

    private Array<Entity> renderQueue;

    public TextureRenderSystem() {
        super(Family.all(TransformComponent.class, TextureComponent.class).get(), new TextureLayerComparator());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        batch = new SpriteBatch();
        Entity entity = engine.getEntitiesFor(Family.one(GameCameraComponent.class).get()).first();
        viewport = entity.getComponent(GameCameraComponent.class);
        renderQueue = new Array<>();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();

        for (Entity entity : renderQueue) {
            TextureComponent texture = getComponent(entity, TextureComponent.class);

            if (texture.getRegion() != null) {
                ParticleEffectComponent particleEffectComponent = entity.getComponent(ParticleEffectComponent.class);
                if (particleEffectComponent != null) {
                    particleEffectComponent.getParticleEffect().draw(batch, deltaTime);
                }

                OpacityComponent opacityComponent = entity.getComponent(OpacityComponent.class);
                if (opacityComponent != null) {
                    batch.setColor(1f, 1f, 1f, opacityComponent.getOpacity());
                }

                ColorComponent colorComponent = entity.getComponent(ColorComponent.class);
                if (colorComponent != null) {
                    batch.setColor(colorComponent.getRed(), colorComponent.getGreen(), colorComponent.getBlue(), 1f);
                }

                TransformComponent transform = getComponent(entity, TransformComponent.class);
                float x = pixel(transform.getPosition().x) - half(transform.getWidth());
                float y = pixel(transform.getPosition().y) - half(transform.getHeight());

                batch.draw(texture.getRegion(), x, y,
                        half(transform.getWidth()), half(transform.getHeight()),
                        transform.getWidth(), transform.getHeight(),
                        transform.getScale().x, transform.getScale().y,
                        MathUtils.radiansToDegrees * transform.getRotation());

                batch.setColor(Color.WHITE);
            }
        }

        batch.end();
        renderQueue.clear();
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }

    public void dispose() {
        batch.dispose();
    }

}
