package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.samplegame.game.basic.component.GameCameraComponent;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.player.component.PlayerComponent;

import static com.samplegame.game.basic.utility.EntityUtility.createAddEntity;
import static com.samplegame.game.basic.utility.EntityUtility.createComponent;
import static com.samplegame.game.basic.utility.MeasureUtility.pixel;

public class GameCameraSystem extends EntitySystem {

    private Viewport viewport;

    private GameCameraComponent gameCamera;

    @Override
    public void addedToEngine(Engine engine) {
        gameCamera = createComponent(engine, GameCameraComponent.class);
        gameCamera.setCamera(new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
        gameCamera.setTarget(EntityUtility.findEntity(engine, PlayerComponent.class));
        createAddEntity(engine, gameCamera);

        viewport = new ScreenViewport(gameCamera.getCamera());
    }

    @Override
    public void update(float deltaTime) {
        Body targetBody = gameCamera.getTarget().getComponent(PhysicComponent.class).getBody();

        gameCamera.getCamera().position.x = pixel(targetBody.getPosition().x);
        gameCamera.getCamera().position.y = pixel(targetBody.getPosition().y);
        gameCamera.getCamera().update();
    }

    public void resize() {
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }
}
