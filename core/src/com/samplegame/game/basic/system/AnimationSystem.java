package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.samplegame.game.basic.component.AnimationComponent;
import com.samplegame.game.basic.component.StateComponent;
import com.samplegame.game.basic.component.TextureComponent;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;

/**
 * Created by otavio on 20/03/2017.
 */
public class AnimationSystem extends IteratingSystem {

    public AnimationSystem() {
        super(Family.all(TextureComponent.class, AnimationComponent.class, StateComponent.class).get());
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        StateComponent state = getComponent(entity, StateComponent.class);

        Animation animation = getComponent(entity, AnimationComponent.class).getAnimations().get(state.getCurrent());
        if (animation != null) {
            getComponent(entity, TextureComponent.class).setRegion((TextureRegion) animation.getKeyFrame(state.getTime()));
        }

        state.addTime(deltaTime);
    }
}
