package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.samplegame.game.basic.component.StateComponent;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;

public class StateSystem extends IteratingSystem {

    public StateSystem() {
        super(Family.all(StateComponent.class).get());
    }

    @Override
    public void processEntity(Entity entity, float deltaTime) {
        getComponent(entity, StateComponent.class).addTime(deltaTime);
    }

}

