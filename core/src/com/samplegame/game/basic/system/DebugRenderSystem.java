package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.samplegame.game.GameScreen;

import static com.samplegame.game.basic.utility.MeasureUtility.PIXELS_BY_METER;

public class DebugRenderSystem extends EntitySystem {

    private Box2DDebugRenderer debugRenderer;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        debugRenderer = new Box2DDebugRenderer();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        Matrix4 debugMatrix = getEngine().getSystem(TextureRenderSystem.class).getBatch().getProjectionMatrix().cpy().scale(PIXELS_BY_METER, PIXELS_BY_METER, 0);
        debugRenderer.render(GameScreen.world, debugMatrix);
    }

}
