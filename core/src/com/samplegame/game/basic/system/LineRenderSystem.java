package com.samplegame.game.basic.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.basic.component.GameCameraComponent;
import com.samplegame.game.basic.component.LineComponent;
import com.samplegame.game.basic.utility.EntityUtility;
import com.samplegame.game.basic.utility.LineLayerComparator;
import com.samplegame.game.basic.utility.TextureLayerComparator;

import static com.samplegame.game.basic.utility.EntityUtility.getComponent;

public class LineRenderSystem extends SortedIteratingSystem {

    private GameCameraComponent viewport;

    private Array<Entity> renderQueue;

    private ShapeRenderer shapeRenderer;

    public LineRenderSystem() {
        super(Family.all(LineComponent.class).get(), new LineLayerComparator());
    }

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        viewport = EntityUtility.findComponent(engine, GameCameraComponent.class);
        renderQueue = new Array<>();
        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);

        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        for (Entity entity : renderQueue) {
            LineComponent line = getComponent(entity, LineComponent.class);
            shapeRenderer.setColor(line.getColor());
            shapeRenderer.line(line.getStartPosition(), line.getEndPosition());
        }

        shapeRenderer.end();
        renderQueue.clear();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        renderQueue.add(entity);
    }
}
