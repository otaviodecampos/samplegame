package com.samplegame.game.item.type;

import com.badlogic.ashley.core.Component;
import com.samplegame.game.physic.collision.CollisionCategory;
import com.samplegame.game.physic.collision.CollisionMask;
import com.samplegame.game.physic.type.BodyModel;
import com.samplegame.game.physic.type.BoxBodyModel;
import com.samplegame.game.physic.type.TriangleBodyModel;
import com.samplegame.game.terrain.component.item.TerrainItemTorchComponent;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;

@Getter
@AllArgsConstructor
public enum Item {

    BROCK_TOP(ItemType.BLOCK, 20, TERRAIN_BLOCK_SIZE, TERRAIN_BLOCK_SIZE, "block/brock_grass", new BoxBodyModel(false, true, 1f, 0.3f, 0.4f, CollisionCategory.ITEM_CATEGORY, CollisionMask.ITEM_MASK), null),
    BROCK(ItemType.BLOCK, 20, TERRAIN_BLOCK_SIZE, TERRAIN_BLOCK_SIZE, "block/brock", new BoxBodyModel(false, true, 1f, 0.3f, 0.4f, CollisionCategory.ITEM_CATEGORY, CollisionMask.ITEM_MASK), null),
    TORCH(ItemType.ITEM, 1, 32, 96, "item/torch/torch", new TriangleBodyModel(false, false, 1f, 0.3f, 0.4f, CollisionCategory.ITEM_CATEGORY, CollisionMask.ITEM_MASK), TerrainItemTorchComponent.class);

    private ItemType type;

    private int resistance;

    private int width;

    private int height;

    private String texturePath;

    private BodyModel bodyModel;

    private Class<? extends Component> terrainItemComponentClass;

}
