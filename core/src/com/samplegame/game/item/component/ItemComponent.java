package com.samplegame.game.item.component;

import com.badlogic.ashley.core.Component;
import com.samplegame.game.basic.component.ClearableComponent;
import com.samplegame.game.item.type.Item;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemComponent implements Component, ClearableComponent {

    private Item type;

    @Override
    public void clear() {
        type = null;
    }

}
