package com.samplegame.game.item.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.samplegame.game.basic.component.DisposableComponent;
import com.samplegame.game.basic.component.GameCameraComponent;
import com.samplegame.game.basic.component.TextureComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.basic.utility.CacheUtility;
import com.samplegame.game.player.component.PlayerComponent;
import com.samplegame.game.player.type.ItemInfo;
import com.samplegame.game.terrain.component.TerrainComponent;
import com.samplegame.game.terrain.component.TerrainLightComponent;
import com.samplegame.game.terrain.type.TerrainItem;
import com.samplegame.game.ui.system.ItemBarSystem;

import static com.samplegame.GameConstants.TERRAIN_BLOCK_SIZE;
import static com.samplegame.game.basic.utility.EntityUtility.*;
import static com.samplegame.game.basic.utility.MeasureUtility.half;
import static com.samplegame.game.basic.utility.MeasureUtility.meter;
import static com.samplegame.game.terrain.utility.TerrainItemCursorUtility.*;

public class TerrainItemActionSystem extends EntitySystem {

    private ItemBarSystem itemBarSystem;

    private PlayerComponent player;

    private GameCameraComponent gameCamera;

    private TerrainComponent terrain;

    private TerrainLightComponent terrainLightComponent;

    @Override
    public void addedToEngine(Engine engine) {
        super.addedToEngine(engine);
        itemBarSystem = engine.getSystem(ItemBarSystem.class);
        player = findComponent(engine, PlayerComponent.class);
        gameCamera = findComponent(engine, GameCameraComponent.class);
        terrain = findComponent(engine, TerrainComponent.class);
        terrainLightComponent = findComponent(engine, TerrainLightComponent.class);
    }

    @Override
    public void update(float deltaTime) {
        super.update(deltaTime);
        if (!itemBarSystem.isMouseDrag() && !itemBarSystem.isMouseHover() && (terrain.getLastDestroyTime() == 0 || System.currentTimeMillis() - terrain.getLastDestroyTime() > 1000)) {
            ItemInfo selectedItem = player.getItem(itemBarSystem.getSelectedItemBox());

            if (selectedItem != null) {
                TerrainItem terrainItem = new TerrainItem(getCursorBlockCol(gameCamera.getCamera()), getCursorBlockRow(gameCamera.getCamera()), selectedItem.getItem());
                if (!terrain.exists(terrainItem)) {
                    TextureComponent texture = createComponent(getEngine(), TextureComponent.class);
                    texture.setRegion(CacheUtility.getTextureRegion(selectedItem.getItem().getTexturePath()));

                    TransformComponent transform = createComponent(getEngine(), TransformComponent.class);
                    transform.setIndex(1);
                    if (terrain.allowedToAdd(terrainItem)) {
                        transform.setHeight(terrainItem.getItem().getHeight());
                        transform.setWidth(terrainItem.getItem().getWidth());
                        transform.getPosition().set(new Vector2(meter(getCursorBlockCol(gameCamera.getCamera()) * TERRAIN_BLOCK_SIZE + half(terrainItem.getItem().getWidth())), meter(getCursorBlockRow(gameCamera.getCamera()) * TERRAIN_BLOCK_SIZE + half(terrainItem.getItem().getHeight()))));
                    } else {
                        transform.setHeight(terrainItem.getItem().getHeight() - terrainItem.getItem().getHeight() / 4);
                        transform.setWidth(terrainItem.getItem().getWidth() - terrainItem.getItem().getWidth() / 4);
                        Vector3 cursorPosition = getCursorInGameCamera(gameCamera.getCamera());
                        transform.getPosition().set(meter(cursorPosition.x - half(terrainItem.getItem().getWidth() - terrainItem.getItem().getWidth() / 4)), meter(cursorPosition.y + half(terrainItem.getItem().getHeight() - terrainItem.getItem().getHeight() / 4)));
                    }

                    getEngine().addEntity(createEntity(getEngine(), DisposableComponent.class, transform, texture));

                    if (Gdx.input.isTouched() && terrain.add(terrainItem)) {
                        player.removeItem(selectedItem, 1);
                        itemBarSystem.setProcessing(true);
                        terrainLightComponent.updateChunkLightSource(terrainItem.getRow(), terrainItem.getCol(), false);
                    }
                }
            }
        }
    }
}
