package com.samplegame.game.player.component;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.utils.Array;
import com.samplegame.game.item.type.Item;
import com.samplegame.game.player.type.ItemInfo;
import lombok.Getter;

import java.util.stream.IntStream;

import static com.samplegame.GameConstants.PLAYER_MAX_ITEMS;

@Getter
public class PlayerComponent implements Component {

    private Array<ItemInfo> items = new Array<>();

    public boolean addItem(Item item) {
        if (items.size == 0) {
            IntStream.range(0, PLAYER_MAX_ITEMS).forEach(i -> items.add(null));
        }
        if (getTotalItems() <= PLAYER_MAX_ITEMS) {
            boolean newItem = getTotalItems() < PLAYER_MAX_ITEMS;
            for (ItemInfo itemInfo : items) {
                if (itemInfo != null && itemInfo.getItem().equals(item) && itemInfo.getQuantity() < 99) {
                    itemInfo.add(1);
                    return true;
                }
            }
            if (newItem) {
                items.set(getFirstEmptyIndex(), new ItemInfo(item));
                return true;
            }
        }
        return false;
    }

    public ItemInfo getItem(int index) {
        try {
            return items.get(index);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    public void removeItem(ItemInfo itemInfo, int quantity) {
        itemInfo.remove(quantity);
        if (itemInfo.getQuantity() < 1) {
            items.set(items.indexOf(itemInfo, true), null);
        }
    }

    public ItemInfo removeItem(int index) {
        ItemInfo itemInfo = items.get(index);
        items.set(index, null);
        return itemInfo;
    }

    private int getFirstEmptyIndex() {
        for (int i = 0; i < items.size; i++) {
            if (items.get(i) == null) {
                return i;
            }
        }
        return 0;
    }

    public int getTotalItems() {
        return (int) IntStream.range(0, items.size).filter(i -> items.get(i) != null).count();
    }

    public void swapItem(int originItemBoxIndex, int targetItemBoxIndex) {
        items.swap(originItemBoxIndex, targetItemBoxIndex);
    }
}
