package com.samplegame.game.player.system;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.ashley.core.Family;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.samplegame.game.basic.component.StateComponent;
import com.samplegame.game.basic.component.TransformComponent;
import com.samplegame.game.physic.component.PhysicComponent;
import com.samplegame.game.player.component.PlayerComponent;

import static com.samplegame.GameConstants.*;

/**
 * Created by otavio on 12/03/2017.
 */
public class PlayerInputSystem extends EntitySystem {

    private PhysicComponent physic;

    private TransformComponent transform;

    private StateComponent state;

    @Override
    public void addedToEngine(Engine engine) {
        Entity entity = engine.getEntitiesFor(Family.all(PlayerComponent.class, TransformComponent.class, PhysicComponent.class).get()).first();
        physic = entity.getComponent(PhysicComponent.class);
        transform = entity.getComponent(TransformComponent.class);
        state = entity.getComponent(StateComponent.class);
    }

    @Override
    public void update(float deltaTime) {
        Body body = physic.getBody();
        Vector2 scale = transform.getScale();

        boolean idle = true;
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            jump(body, scale);
            idle = false;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A)) {
            moveLeft(body, scale);
            idle = false;
            state.setCurrent(PLAYER_STATE_RUN);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D)) {
            moveRight(body, scale);
            idle = false;
            state.setCurrent(PLAYER_STATE_RUN);
        }

        if (idle) {
            stop(body, scale);
            state.setCurrent(PLAYER_STATE_IDLE);
        }

        if (body.getLinearVelocity().y > 0) {
            state.setCurrent(PLAYER_STATE_JUMP);
        } else if (body.getLinearVelocity().y < 0) {
            state.setCurrent(PLAYER_STATE_FALL);
        }

    }

    private void jump(Body body, Vector2 scale) {
        if (body.getLinearVelocity().y == 0.0f) {
            body.applyLinearImpulse(0.0f, 1.0f * PLAYER_JUMP_VELOCITY * body.getMass(), body.getWorldCenter().x, body.getWorldCenter().y, true);
        }
    }

    private void moveLeft(Body body, Vector2 scale) {
        if (Math.signum(body.getLinearVelocity().x) == 1) {
            stop(body, scale);
        }

        if (body.getLinearVelocity().x > -PLAYER_MAX_MOVE_VELOCITY.x) {
            body.applyForce(-PLAYER_LINEAR_VELOCITY, 0, body.getPosition().x, body.getPosition().y, true);
            scale.x = Math.abs(scale.x) * -1f;
        }
    }

    private void moveRight(Body body, Vector2 scale) {
        if (Math.signum(body.getLinearVelocity().x) == -1) {
            stop(body, scale);
        }
        if (body.getLinearVelocity().x < PLAYER_MAX_MOVE_VELOCITY.x) {
            body.applyForce(PLAYER_LINEAR_VELOCITY, 0, body.getPosition().x, body.getPosition().y, true);
            scale.x = Math.abs(scale.x) * 1f;
        }
    }

    private void stop(Body body, Vector2 scale) {
        body.setLinearVelocity(body.getLinearVelocity().x * 0.8f, body.getLinearVelocity().y);
    }

}
