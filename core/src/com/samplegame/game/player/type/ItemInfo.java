package com.samplegame.game.player.type;

import com.samplegame.game.item.type.Item;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ItemInfo {

    @NonNull
    private Item item;

    private int quantity = 1;

    public void add(int quantity) {
        this.quantity += quantity;
    }

    public void remove(int quantity) {
        this.quantity -= quantity;
    }

}
