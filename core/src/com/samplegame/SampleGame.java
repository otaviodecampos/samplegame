package com.samplegame;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.samplegame.game.GameScreen;
import lombok.Getter;

@Getter
public class SampleGame extends Game {

    public static boolean debug;

    public SampleGame(Boolean debug) {
        SampleGame.debug = debug;
    }

    @Override
    public void create() {
        setScreen(new GameScreen());
    }

    @Override
    public void render() {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0, 0, 0, 0);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        this.screen.dispose();
    }
}
