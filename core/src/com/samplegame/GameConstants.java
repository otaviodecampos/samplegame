package com.samplegame;

import com.badlogic.gdx.math.Vector2;

public class GameConstants {

    public static final Vector2 WORLD_SIZE = new Vector2(1000, 100);

    public static final int TERRAIN_BLOCK_SIZE = 32;

    public static final int HALF_TERRAIN_BLOCK_SIZE = TERRAIN_BLOCK_SIZE / 2;

    public static final int WORLD_INITIAL_PLAYER_ROW = 15;

    public static final int PLAYER_WIDTH = 60;

    public static final int PLAYER_HEIGHT = 70;

    public static final int PLAYER_TEXTURE_HEIGHT = 90;

    public static final float PLAYER_LINEAR_VELOCITY = 2.0f;

    public static final Vector2 PLAYER_MAX_MOVE_VELOCITY = new Vector2(3.3f, 2.99f);

    public static final float PLAYER_JUMP_VELOCITY = 4.33f;

    public static final int PLAYER_STATE_IDLE = 0;

    public static final int PLAYER_STATE_RUN = 1;

    public static final int PLAYER_STATE_JUMP = 2;

    public static final int PLAYER_STATE_FALL = 3;

    public static final int PLAYER_MAX_ITEMS = 6;

}
