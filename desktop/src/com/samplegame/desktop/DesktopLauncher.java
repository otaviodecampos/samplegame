package com.samplegame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.samplegame.SampleGame;

public class DesktopLauncher {
    public static void main(String[] arg) {
        if (Boolean.valueOf(System.getProperty("pack"))) {
            AssetsPacker.main(arg);
        }
        System.getProperty("debug");
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Sample Game";
        config.width = 1280;
        config.height = 720;
        new LwjglApplication(new SampleGame(Boolean.valueOf(System.getProperty("debug"))), config);
        //new LwjglApplication(new TestParticle(), config);
    }
}
