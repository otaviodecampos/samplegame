package com.samplegame.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

public class AssetsPacker {

    public static void main(String[] args) {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        TexturePacker.process(settings, "../../assets", "player", "assets");
    }

}
